# TP3


### Tableau d'adressage

| Machine          | Réseau 1        | Réseau 2        | Réseau 3        |
| ---------------- | --------------- | --------------- | --------------- |
| `node1.net1.tp3` | `10.3.1.11/24`  | nop             | nop             |
| `node2.net1.tp3` | `10.3.1.12/24`  | nop             | nop             |
| `router1.tp3`    | `10.3.1.254/24` | nop             | `10.3.100.1/30` |
| `router2.tp3`    | nop             | `10.3.2.254/24` | `10.3.100.2/30` |
| `node1.net2.tp3` | nop             | `10.3.2.11/24`  | nop             |
| `node2.net2.tp3` | nop             | `10.3.2.12/24`  | nop             |

## I. Setup GNS3

**Configurer le node1**

```bash
[user@node1 ~]$ sudo echo 'node1.net1.tp3'|sudo tee /etc/hostname

[user@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3


NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.11
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
DNS1=1.1.1.1

[user@node1 ~]$ sudo nmcli con reload
[user@node1 ~]$ sudo nmcli con up enp0s3

[user@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:92:66:45 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe92:6645/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:15:bd:09 brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.14/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 401sec preferred_lft 401sec
    inet6 fe80::c115:6b22:b049:3201/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```


**Configurer le node2**

```bash
[user@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:6a:58:83 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe6a:5883/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:be:57:63 brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.13/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 579sec preferred_lft 579sec
    inet6 fe80::48fb:d1d1:e511:c706/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Configurer le router 1

```bash
[user@router1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes


[user@router1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s9

NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.254
NETMASK=255.255.255.0

GATEWAY=192.168.171.136
DNS1=1.1.1.1


[user@router1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s10                        
NAME=enp0s10
DEVICE=enp0s10

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.100.1
NETMASK=255.255.255.252

GATEWAY=192.168.171.136
DNS1=1.1.1.1


[user@router1 ~]$ nmcli con reload
[user@router1 ~]$ sudo nmcli con up enp0s3
[user@router1 ~]$ sudo nmcli con up enp0s9
[user@router1 ~]$ sudo nmcli con up enp0s10

[user@router1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1e:1d:a9 brd ff:ff:ff:ff:ff:ff
    inet 192.168.171.136/24 brd 192.168.171.255 scope global dynamic noprefixroute enp0s3
       valid_lft 1352sec preferred_lft 1352sec
    inet6 fe80::a00:27ff:fe1e:1da9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:75:a7:51 brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.17/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 417sec preferred_lft 417sec
    inet6 fe80::5176:38d1:2663:f19/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:06:ea:8f brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.254/24 brd 10.3.1.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe06:ea8f/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e6:77:53 brd ff:ff:ff:ff:ff:ff
    inet 10.3.100.1/30 brd 10.3.100.3 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee6:7753/64 scope link
       valid_lft forever preferred_lft forever

```

**Test routeur 1**

```bash

[user@router1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=24.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=19.2 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 19.167/22.004/24.841/2.837 ms


[user@router1 ~]$ ping efrei.fr
PING efrei.fr (51.255.68.208) 56(84) bytes of data.
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=1 ttl=128 time=21.4 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=2 ttl=128 time=21.0 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=3 ttl=128 time=26.0 ms
^C
--- efrei.fr ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 21.021/22.793/25.987/2.262 ms
```

**Configurer le routeur 2**

```bash
[user@router2 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.100.2
NETMASK=255.255.255.252

GATEWAY=192.168.171.136
DNS1=1.1.1.1


[user@router2 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s9                      
NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.254
NETMASK=255.255.255.0

GATEWAY=10.3.100.2
DNS1=1.1.1.1


[user@router2 ~]$ sudo nmcli con reload
[user@router2 ~]$ sudo nmcli con up enp0s3
[user@router2 ~]$ sudo nmcli con up enp0s9

[user@router2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:be:92:3e brd ff:ff:ff:ff:ff:ff
    inet 10.3.100.2/30 brd 10.3.100.3 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febe:923e/64 scope link tentative
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:52:57:9a brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.16/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 590sec preferred_lft 590sec
    inet6 fe80::2d91:fac2:61eb:4ebd/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:35:de:ef brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.254/24 brd 10.3.2.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe35:deef/64 scope link
       valid_lft forever preferred_lft forever

```

Configurer le node 1 du LAN 2

```bash
[user@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.11
NETMASK=255.255.255.0

GATEWAY=10.3.2.254
DNS1=1.1.1.1

[user@node1 ~]$ sudo nmcli con reload
[user@node1 ~]$ sudo nmcli con up enp0s3

[user@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1d:db:0e brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.11/24 brd 10.3.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe1d:db0e/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c9:ac:0c brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.12/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 454sec preferred_lft 454sec
    inet6 fe80::b580:7645:68e7:35a3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```



Configurer le node 2 du LAN 2

```bash
[user@node2 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.12
NETMASK=255.255.255.0

GATEWAY=10.3.2.254
DNS1=1.1.1.1

[user@node2 ~]$ sudo nmcli con reload
[user@node2 ~]$ sudo nmcli con up enp0s3

[user@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:bc:cd:90 brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.12/24 brd 10.3.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febc:cd90/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:00:0d:c6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.15/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 345sec preferred_lft 345sec
    inet6 fe80::8bdd:2bdd:9af2:4e18/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

**Teste de connexion au LAN 1 10.3.1.0/24**

```bash
[user@node1 ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=2.53 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=2.04 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=2.46 ms
^C
--- 10.3.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 2.043/2.344/2.525/0.214 ms

[user@node1 ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=2.83 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=2.14 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=2.96 ms
^C
--- 10.3.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2505ms
rtt min/avg/max/mdev = 2.824/2.138/2.957/0.314 ms

[user@node2 ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=3.38 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=2.47 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=1.73 ms
64 bytes from 10.3.1.254: icmp_seq=4 ttl=64 time=1.73 ms
^C
--- 10.3.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 1.729/2.328/3.379/0.678 ms
```


**Teste de connexion au LAN 2 10.3.2.0/24**

```bash
[user@node2 ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=64 time=8.81 ms
64 bytes from 10.3.2.11: icmp_seq=2 ttl=64 time=2.84 ms
64 bytes from 10.3.2.11: icmp_seq=3 ttl=64 time=1.90 ms
^C
--- 10.3.2.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.903/4.516/8.805/3.056 ms



[user@node1 ~]$ ping 10.3.2.254
PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=4.03 ms
64 bytes from 10.3.2.254: icmp_seq=2 ttl=64 time=2.21 ms
64 bytes from 10.3.2.254: icmp_seq=3 ttl=64 time=1.31 ms
^C
--- 10.3.2.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 1.307/2.513/4.025/1.130 ms



[user@node2 ~]$ ping 10.3.2.254
PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=4.82 ms
64 bytes from 10.3.2.254: icmp_seq=2 ttl=64 time=1.61 ms
64 bytes from 10.3.2.254: icmp_seq=3 ttl=64 time=1.49 ms
^C
--- 10.3.2.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.491/2.639/4.815/1.538 ms
```

**Teste de connexion au LAN 3 10.3.100.0/30**

```bash
[user@router2 ~]$ ping 10.3.100.1
PING 10.3.100.1 (10.3.100.1) 56(84) bytes of data.
64 bytes from 10.3.100.1: icmp_seq=1 ttl=64 time=3.64 ms
64 bytes from 10.3.100.1: icmp_seq=2 ttl=64 time=5.52 ms
64 bytes from 10.3.100.1: icmp_seq=3 ttl=64 time=1.44 ms
^C
--- 10.3.100.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 1.442/3.534/5.524/1.668 ms
```


## II. Routes routes routes

**Ajouter route pour node 1**
```bash
[user@node1 ~]$ ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.14 metric 101


[user@node1 ~]$ sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s3

[user@node1 ~]$ sudo ip route add default via 10.3.1.254 dev enp0s3

[user@node1 ~]$ ip r s
default via 10.3.1.254 dev enp0s3
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s3
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.14 metric 101

[user@node1 ~]$ echo 'GATEWAY=10.3.1.254'| sudo tee /etc/sysconfig/network
```

**Ajouter route pour node 1**
```bash
[user@node2 ~]$ ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.14 metric 101


[user@node2 ~]$ sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s3

[user@node2 ~]$ sudo ip route add default via 10.3.1.254 dev enp0s3

[user@node2 ~]$ ip r s
default via 10.3.1.254 dev enp0s3
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s3
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.13 metric 101

[user@node2 ~]$ echo 'GATEWAY=10.3.1.254'| sudo tee /etc/sysconfig/network
```

**Ajouter route pour node 1 LAN 2**
```bash
[user@node1 ~]$ ip r s
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.11 metric 100
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.12 metric 101


[user@node1 ~]$ sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s3

[user@node1 ~]$ sudo ip route add default via 10.3.2.254 dev enp0s3

[user@node1 ~]$ ip r s
default via 10.3.2.254 dev enp0s3
10.3.1.0/24 via 10.3.2.254 dev enp0s3
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.11 metric 100
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.12 metric 101

[user@node1 ~]$ echo 'GATEWAY=10.3.2.254'| sudo tee /etc/sysconfig/network
```
**Ajouter route pour node 2 LAN 2**
```bash
[user@node2 ~]$ ip r s
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.11 metric 100
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.12 metric 101


[user@node2 ~]$ sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s3

[user@node2 ~]$ sudo ip route add default via 10.3.2.254 dev enp0s3

[user@node2 ~]$ ip  r s
default via 10.3.2.254 dev enp0s3
10.3.1.0/24 via 10.3.2.254 dev enp0s3
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 100
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.15 metric 101

[user@node2 ~]$ echo 'GATEWAY=10.3.2.254'| sudo tee /etc/sysconfig/network
```
**Ajouter route pour router 1**
```bash
[user@router1 ~]$ sudo ip route add 10.3.2.0/24 via 10.3.100.2 dev enp0s10

[user@router1 ~]$ ip r s
default via 192.168.171.2 dev enp0s3 proto dhcp src 192.168.171.136 metric 101
10.3.1.0/24 dev enp0s9 proto kernel scope link src 10.3.1.254 metric 103
10.3.2.0/24 via 10.3.100.2 dev enp0s10
10.3.100.0/30 dev enp0s10 proto kernel scope link src 10.3.100.1 metric 100
192.168.171.0/24 dev enp0s3 proto kernel scope link src 192.168.171.136 metric 101
192.168.171.136 dev enp0s10 proto static scope link metric 100
192.168.171.136 dev enp0s9 proto static scope link metric 103
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.17 metric 102

```

**Ajouter route pour router 2**

```bash 
[user@router2 ~]$ sudo ip route add 10.3.1.0/24 via 10.3.100.1 dev enp0s3

[user@router2 ~]$ sudo ip route add default via 10.3.100.1 dev enp0s3

[user@router2 ~]$ ip r s
default via 10.3.100.1 dev enp0s3
10.3.1.0/24 via 10.3.100.1 dev enp0s3
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.254 metric 102
10.3.100.0/30 dev enp0s3 proto kernel scope link src 10.3.100.2 metric 100
10.3.100.2 dev enp0s9 proto static scope link metric 102
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.16 metric 101

[user@router2 ~]$ echo 'GATEWAY=10.3.100.1'| sudo tee /etc/sysconfig/network
```
**Test connexion**

node1 -> internet
```bash
[user@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=22.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=19.5 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 19.526/20.933/22.341/1.407 m
```
Traceroute
```bash
[user@node2 ~]$ traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.2.254)  2.200 ms  1.944 ms  0.788 ms
 2  10.3.100.1 (10.3.100.1)  2.823 ms  2.841 ms  3.175 ms
 3  192.168.171.2 (192.168.171.2)  5.096 ms  5.948 ms  5.323 ms
```

# I. DHCP

**Configuration du serveur DHCP**
```bash
[user@node1 ~]$ sudo nano /etc/hostname

dhcp.net1.tp3

[user@dhcp ~]$
dnf -y install dhcp-server

[user@dhcp ~]$ nano /etc/dhcp/dhcpd.conf

default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.0 {
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    option broadcast-address 10.3.1.255;
    option routers 10.3.1.254;
    option domain-name-servers 1.1.1.1;
}

[user@dhcp ~]$ sudo systemctl enable --now dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.

[user@dhcp ~]$ sudo firewall-cmd --add-service=dhcp
success

[user@dhcp ~]$ sudo firewall-cmd --runtime-to-permanent
success
```
**Vérifiation que le dhcp marche sur le node 1**
```bash
[user@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:92:66:45 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 597sec preferred_lft 597sec
    inet6 fe80::a00:27ff:fe92:6645/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:15:bd:09 brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.14/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 505sec preferred_lft 505sec
    inet6 fe80::c115:6b22:b049:3201/64 scope link noprefixroute
       valid_lft forever preferred_lft forever


[user@node1 ~]$ ip r s
default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 102
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 102
10.3.2.0/24 via 10.3.1.254 dev enp0s3
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.14 metric 101


[user@node1 ~]$ dig 1.1.1.1

; <<>> DiG 9.16.23-RH <<>> 1.1.1.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 35701
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;1.1.1.1.                       IN      A

;; AUTHORITY SECTION:
.                       85680   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2023100600 1800 900 604800 86400

;; Query time: 28 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 08:42:12 CEST 2023
;; MSG SIZE  rcvd: 111


[user@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=23.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=25.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=38.5 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2008ms
rtt min/avg/max/mdev = 23.925/29.405/38.533/6.497 ms
```
# SERVER WEB

**Intallation nginx**
```bash
[user@web ~]$ sudo dnf install nginx
```
**Création page racine web**
```bash
[user@web ~]$ cd /
[user@web /]$ ls
afs  bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
[user@web /]$ cd var
[user@web var]$ cd www
-bash: cd: www: No such file or directory
[user@web var]$ ls
adm    crash  empty  games     lib    lock  mail  opt       run    tmp
cache  db     ftp    kerberos  local  log   nis   preserve  spool  yp
[user@web var]$ sudo mkdir www
[user@web var]$ cd www
[user@web www]$ sudo mkdir efrei_site_nul
[user@web www]$ ls
efrei_site_nul



[user@web www]$ sudo chown nginx efrei_site_nul/
[user@web www]$ ls -a
.  ..  efrei_site_nul
[user@web www]$ ls -all
total 4
drwxr-xr-x.  3 root  root   28 Oct  6 08:52 .
drwxr-xr-x. 20 root  root 4096 Oct  6 08:51 ..
drwxr-xr-x.  2 nginx root    6 Oct  6 08:52 efrei_site_nul
[user@web www]$ cd efrei_site_nul/
[user@web efrei_site_nul]$ sudo touch index.html
[user@web efrei_site_nul]$ sudo nano index.html
[user@web efrei_site_nul]$ ls -all
total 4
drwxr-xr-x. 2 nginx root 24 Oct  6 08:57 .
drwxr-xr-x. 3 root  root 28 Oct  6 08:52 ..
-rw-r--r--. 1 nginx root 15 Oct  6 08:58 index.html
[user@web efrei_site_nul]$
```
# Config de NGINX
```bash
[user@web efrei_site_nul]$ sudo nano /etc/nginx/conf.d/web.net2.tp3.conf


  server {
      server_name   web.net2.tp3;

      listen        10.3.2.101:80;

      location      / {

          root      /var/www/efrei_site_nul;

          index index.html;
      }
  }

```
**Firewall**
```bash
[user@web efrei_site_nul]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[user@web efrei_site_nul]$ sudo firewall-cmd --reload
success
[user@web efrei_site_nul]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
**Test**
```bash
[user@web efrei_site_nul]$ sudo systemctl start nginx
[user@web efrei_site_nul]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[user@web efrei_site_nul]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset: disabled)
     Active: active (running) since Fri 2023-10-06 09:07:42 CEST; 17s ago
   Main PID: 11409 (nginx)
      Tasks: 2 (limit: 4999)
     Memory: 2.0M
        CPU: 41ms
     CGroup: /system.slice/nginx.service
             ├─11409 "nginx: master process /usr/sbin/nginx"
             └─11410 "nginx: worker process"

Oct 06 09:07:42 node1.net2.tp3 systemd[1]: Starting The nginx HTTP and reverse proxy server...
Oct 06 09:07:42 node1.net2.tp3 nginx[11407]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Oct 06 09:07:42 node1.net2.tp3 nginx[11407]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Oct 06 09:07:42 node1.net2.tp3 systemd[1]: Started The nginx HTTP and reverse proxy server.
```

**Test node 1**
```bash
[user@node1 ~]$ curl http://10.3.2.101
un site au pif

[user@node1 ~]$ sudo nano /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.3.2.101  web.net2.tp3

[user@node1 ~]$ curl http://web.net2.tp3
un site au pif
```

# Serveur DNS

**Config**
```bash
[user@dns ~]$ sudo nano /etc/named.conf

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any;};
        allow-query-cache { localhost; any;};

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};
zone "net2.tp3" IN {
     type master;
     file "net2.tp3.db";
     allow-update { none; };
     allow-query {any; };
};

zone "2.3.10.in-addr.arpa" IN {
     type master;
     file "net2.tp3.rev";
     allow-update { none; };
     allow-query { any; };
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";


[user@dns ~]$ sudo nano /var/named/net2.tp3.db

$TTL 86400
@ IN SOA dns.net2.tp3. admin.net2.tp3. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.net2.tp3.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns        IN A 10.3.2.102
web        IN A 10.3.2.101


[user@dns ~]$ sudo nano /var/named/net2.tp3.rev

$TTL 86400
@ IN SOA dns.net2.tp3. admin.net2.tp3. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.net2.tp3.

;Reverse lookup for Name Server
102   IN PTR dns.net2.tp3.
101   IN PTR web.net2.tp3.


[user@dns ~]$ sudo systemctl start named
[user@dns ~]$ sudo systemctl enable named
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[user@dns ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset: disabled)
     Active: active (running) since Fri 2023-10-06 09:34:15 CEST; 19s ago
   Main PID: 2350 (named)
      Tasks: 4 (limit: 4999)
     Memory: 16.2M
        CPU: 87ms
     CGroup: /system.slice/named.service
             └─2350 /usr/sbin/named -u named -c /etc/named.conf

Oct 06 09:34:15 node2.net2.tp3 named[2350]: zone net2.tp3/IN: loaded serial 2019061800
Oct 06 09:34:15 node2.net2.tp3 named[2350]: network unreachable resolving './DNSKEY/IN': 2001:500:200::b#53
Oct 06 09:34:15 node2.net2.tp3 named[2350]: network unreachable resolving './NS/IN': 2001:500:200::b#53
Oct 06 09:34:15 node2.net2.tp3 named[2350]: zone localhost/IN: loaded serial 0
Oct 06 09:34:15 node2.net2.tp3 named[2350]: zone 1.0.0.127.in-addr.arpa/IN: loaded serial 0
Oct 06 09:34:15 node2.net2.tp3 named[2350]: all zones loaded
Oct 06 09:34:15 node2.net2.tp3 systemd[1]: Started Berkeley Internet Name Domain (DNS).
Oct 06 09:34:15 node2.net2.tp3 named[2350]: running
Oct 06 09:34:15 node2.net2.tp3 named[2350]: managed-keys-zone: Initializing automatic trust anchor management for zo>
Oct 06 09:34:15 node2.net2.tp3 named[2350]: resolver priming query complete
```

**Firewall** 

```bash
[user@dns ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[user@dns ~]$ sudo firewall-cmd --reload
success
[user@dns ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 53/udp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
**Test**
```bash
[user@node1 ~]$  dig efrei.fr

; <<>> DiG 9.16.23-RH <<>> efrei.fr
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 13003
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;efrei.fr.                      IN      A

;; ANSWER SECTION:
efrei.fr.               10800   IN      A       51.255.68.208

;; Query time: 28 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 09:38:23 CEST 2023
;; MSG SIZE  rcvd: 53

[user@node1 ~]$ dig efrei.fr @1.1.1.1

; <<>> DiG 9.16.23-RH <<>> efrei.fr @1.1.1.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20389
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;efrei.fr.                      IN      A

;; ANSWER SECTION:
efrei.fr.               10800   IN      A       51.255.68.208

;; Query time: 31 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 09:38:40 CEST 2023
;; MSG SIZE  rcvd: 53



[user@node1 ~]$ dig -x 10.3.2.101

; <<>> DiG 9.16.23-RH <<>> -x 10.3.2.101
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 3449
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;101.2.3.10.in-addr.arpa.       IN      PTR

;; Query time: 27 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 09:39:11 CEST 2023
;; MSG SIZE  rcvd: 52




[user@node1 ~]$ dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53878
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 66fa2989b47139b901000000651fc9e831134016d542cf36 (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 9 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Fri Oct 06 10:48:40 CEST 2023
;; MSG SIZE  rcvd: 85




[user@node1 ~]$ sudo cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
[user@node1 ~]$

[user@node1 ~]$ nano etc/sysconfig/network-scripts/ifcfg-enp0s3

NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=dhcp
ONBOOT=yes

DNS1=10.3.2.102

[user@node1 ~]$ sudo nmcli con reload
[user@node1 ~]$ sudo nmcli con up enp0s3


[user@node1 ~]$ curl http://web.net2.tp3
un site au pif
[user@node1 ~]$
```

**DHCP**

```bash
[user@dhcp ~]$ sudo nano /etc/dhcp/dhcpd.conf

[user@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
    option domain-name-servers 10.3.2.102;
}

[user@node1 ~]$ dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7506
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 6aeee83153eca26b01000000651fe32784108cc8d3cba02c (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 10 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Fri Oct 06 12:36:23 CEST 2023
;; MSG SIZE  rcvd: 85

``` 