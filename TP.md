# TP2 : Routage, DHCP et DNS

# Sommaire

- [TP2 : Routage, DHCP et DNS](#tp2--routage-dhcp-et-dns)
- [Sommaire](#sommaire)
- [I. Routage](#i-routage)
- [II. Serveur DHCP](#ii-serveur-dhcp)
- [III. ARP](#iii-arp)
  - [1. Les tables ARP](#1-les-tables-arp)
  - [2. ARP poisoning](#2-arp-poisoning)


# I. Routage

**Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `10.2.1.1/24`   |

**Reproduisez la topologie dans votre GNS3**, quelques hints :

- il faudra indiquer à GNS que votre `router.tp2.efrei` a une carte réseau supplémentaire
- le NAT est disponible dans la catégorie "End Devices"
  - il va symboliser un accès internet

**Configuration de `router.tp2.efrei`**

Mettre une ip dynamic

```bash 

[user@nrouter ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s9 

``` 

 
```bash 
NAME=enp0s9 

DEVICE=enp0s9 

  

BOOTPROTO=dhcp 

ONBOOT=yes 

```
Test access 

```bash
[user@router ~]$ ping google.com 

PING google.com (142.250.179.78) 56(84) bytes of data. 

64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=128 time=31.9 ms 

64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=128 time=36.9 ms 

64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=128 time=39.1 ms 

64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=4 ttl=128 time=33.9 ms 

64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=5 ttl=128 time=43.2 ms 

^C 

--- google.com ping statistics --- 

6 packets transmitted, 6 received, 0% packet loss, time 5010ms 

rtt min/avg/max/mdev = 31.862/36.367/43.245/3.903 ms 

``` 

Mettre une ip static

```bash 

[user@router ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3 

``` 
``` bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.254
NETMASK=255.255.255.0

# La suite est optionnelle
GATEWAY=192.168.1.254
DNS1=1.1.1.1
``` 

 
```bash
[user@router ~]$ ip a 

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000 

    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00 

    inet 127.0.0.1/8 scope host lo 

       valid_lft forever preferred_lft forever 

    inet6 ::1/128 scope host 

       valid_lft forever preferred_lft forever 

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:a5:90:57 brd ff:ff:ff:ff:ff:ff 

    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3 

       valid_lft forever preferred_lft forever 

    inet6 fe80::a00:27ff:fea5:9057/64 scope link 

       valid_lft forever preferred_lft forever 

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:fc:31:c3 brd ff:ff:ff:ff:ff:ff 

    inet 192.168.214.9/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8 

       valid_lft 460sec preferred_lft 460sec 

    inet6 fe80::f3dc:3a43:e115:e524/64 scope link noprefixroute 

       valid_lft forever preferred_lft forever 

4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:60:25:b9 brd ff:ff:ff:ff:ff:ff 

    inet 192.168.171.134/24 brd 192.168.171.255 scope global dynamic noprefixroute enp0s9 

       valid_lft 1360sec preferred_lft 1360sec 

    inet6 fe80::f139:9f82:8ea1:3c6a/64 scope link noprefixroute 

       valid_lft forever preferred_lft forever 

``` 

 
```bash
[user@router ~]$ sudo sysctl -w net.ipv4.ip_forward=1 
net.ipv4.ip_forward = 1

[user@router ~]$ sudo firewall-cmd --add-masquerade
success

[user@router ~]$ sudo firewall-cmd --add-masquerade --permanent
success
```

**Configuration de `node1.tp2.efrei`**

```bash

[user@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3 

``` 
```bash 

[user@nrouter ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3 

``` 
``` bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

# La suite est optionnelle
GATEWAY=10.2.1.254
DNS1=1.1.1.1
``` 

 
```bash
[user@node1 ~]$ ip route show 

default via 192.168.1.254 dev enp0s3 proto static metric 102 

10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.1 metric 102 

192.168.1.254 dev enp0s3 proto static scope link metric 102 

192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.8 metric 101 

``` 

 
```bash
[user@node1 ~]$ ping 10.2.1.254 

PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data. 

64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=5.44 ms 

64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=2.77 ms 

64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=2.03 ms 

64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=4.41 ms 

64 bytes from 10.2.1.254: icmp_seq=5 ttl=64 time=2.15 ms 

^C 

--- 10.2.1.254 ping statistics --- 

5 packets transmitted, 5 received, 0% packet loss, time 4009ms 

rtt min/avg/max/mdev = 2.025/3.360/5.443/1.344 ms 

``` 

 
```bash
[user@node1 ~]$ ip r s 

default via 10.2.1.254 dev enp0s3 proto static metric 100 

10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.1 metric 100 

192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.8 metric 101 

``` 

 
```bash
[user@node1 ~]$ ping 8.8.8.8 

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data. 

64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=33.2 ms 

64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=36.3 ms 

64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=33.8 ms 

64 bytes from 8.8.8.8: icmp_seq=4 ttl=127 time=34.2 ms 

64 bytes from 8.8.8.8: icmp_seq=5 ttl=127 time=36.3 ms 

^C 

--- 8.8.8.8 ping statistics --- 

5 packets transmitted, 5 received, 0% packet loss, time 4011ms 

rtt min/avg/max/mdev = 33.166/34.764/36.347/1.309 ms 

``` 

 
```bash
[user@node1 ~]$ traceroute google.com 

traceroute to google.com (142.250.201.174), 30 hops max, 60 byte packets 

1  _gateway (10.2.1.254)  2.680 ms  3.187 ms  1.650 ms 

2  192.168.171.2 (192.168.171.2)  2.224 ms  3.007 ms  2.125 ms^C 

 ```


# II. Serveur DHCP


**Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `N/A`           |
| `dhcp.tp2.efrei`   | `10.2.1.253/24` |

**Install et conf du serveur DHCP** sur `dhcp.tp2.efrei`

Ajout de route ip
```bash
[user@dhcp ~]$ ip r s 

default via 10.2.1.254 dev enp0s3 proto static metric 100 

10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.253 metric 100 

192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.10 metric 101 

``` 
Test Access
 
```bash
[user@dhcp ~]$ ping 8.8.8.8 

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data. 

64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=157 ms 

64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=123 ms 

^C 

--- 8.8.8.8 ping statistics --- 

2 packets transmitted, 2 received, 0% packet loss, time 1002ms 

rtt min/avg/max/mdev = 122.758/139.702/156.647/16.944 ms 
``` 

Configuration serveur dhcp
 
```bash

[user@dhcp ~]$ sudo dnf -y install dhcp-server 

[sudo] password for user: 

Rocky Linux 9 - BaseOS                                                               4.1 kB/s | 4.1 kB     00:00 

Rocky Linux 9 - BaseOS                                                               950 kB/s | 1.9 MB     00:02 

Rocky Linux 9 - AppStream                                                            5.7 kB/s | 4.5 kB     00:00 

Rocky Linux 9 - AppStream                                                            1.4 MB/s | 7.1 MB     00:05 

Rocky Linux 9 - Extras                                                               3.4 kB/s | 2.9 kB     00:00 

Rocky Linux 9 - Extras                                                                11 kB/s |  11 kB     00:00 

Dependencies resolved. 

===================================================================================================================== 

Package                     Architecture           Version                             Repository              Size 

===================================================================================================================== 

Installing: 

dhcp-server                 x86_64                 12:4.4.2-18.b1.el9                  baseos                 1.2 M 

Installing dependencies: 

dhcp-common                 noarch                 12:4.4.2-18.b1.el9                  baseos                 128 k 

  

Transaction Summary 

===================================================================================================================== 

Install  2 Packages 

  

Total download size: 1.3 M 

Installed size: 4.2 M 

Downloading Packages: 

(1/2): dhcp-common-4.4.2-18.b1.el9.noarch.rpm                                        361 kB/s | 128 kB     00:00 

(2/2): dhcp-server-4.4.2-18.b1.el9.x86_64.rpm                                        1.4 MB/s | 1.2 MB     00:00 

--------------------------------------------------------------------------------------------------------------------- 

Total                                                                                831 kB/s | 1.3 MB     00:01 

Running transaction check 

Transaction check succeeded. 

Running transaction test 

Transaction test succeeded. 

Running transaction 

  Preparing        :                                                                                             1/1 

  Installing       : dhcp-common-12:4.4.2-18.b1.el9.noarch                                                       1/2 

  Running scriptlet: dhcp-server-12:4.4.2-18.b1.el9.x86_64                                                       2/2 

  Installing       : dhcp-server-12:4.4.2-18.b1.el9.x86_64                                                       2/2 

  Running scriptlet: dhcp-server-12:4.4.2-18.b1.el9.x86_64                                                       2/2 

  Verifying        : dhcp-server-12:4.4.2-18.b1.el9.x86_64                                                       1/2 

  Verifying        : dhcp-common-12:4.4.2-18.b1.el9.noarch                                                       2/2 

  

Installed: 

  dhcp-common-12:4.4.2-18.b1.el9.noarch                     dhcp-server-12:4.4.2-18.b1.el9.x86_64 

  

Complete! 
``` 

 
```bash
[user@dhcp ~]$ sudo nano /etc/dhcp/dhcpd.conf 

 

option domain-name     "srv.world"; 
option domain-name-servers     dlp.srv.world; 
default-lease-time 600; 
max-lease-time 7200; 
authoritative; 
subnet 10.2.1.0 netmask 255.255.255.0 { 
    range dynamic-bootp 10.2.1.200 10.2.1.252; 
    option broadcast-address 10.2.1.255; 
    option routers 10.2.1.254; 
} 
``` 

 
```bash
[user@dhcp ~]$ sudo systemctl enable --now dhcpd 

Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service. 

[user@dhcp ~]$ sudo firewall-cmd --add-service=dhcp 

success
 
[user@dhcp ~]$ sudo firewall-cmd --runtime-to-permanent 

Success 
``` 

 
**Test du DHCP** sur `node1.tp2.efrei`


```bash
[user@node1 ~]$ ip a 

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000 

    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00 

    inet 127.0.0.1/8 scope host lo 

       valid_lft forever preferred_lft forever 

    inet6 ::1/128 scope host 

       valid_lft forever preferred_lft forever 

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:41:bd:c2 brd ff:ff:ff:ff:ff:ff 

    inet 10.2.1.1/24 brd 10.2.1.255 scope global noprefixroute enp0s3 

       valid_lft forever preferred_lft forever 

    inet6 fe80::a00:27ff:fe41:bdc2/64 scope link 

       valid_lft forever preferred_lft forever 

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:13:ed:18 brd ff:ff:ff:ff:ff:ff 

    inet 192.168.214.8/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8 

       valid_lft 486sec preferred_lft 486sec 

    inet6 fe80::301f:cd97:b9cb:1380/64 scope link noprefixroute 

       valid_lft forever preferred_lft forever 

``` 

 
```bash 
[user@node1 ~]$ sudo nmcli con del enp0s3 

[sudo] password for user: 

Connection 'enp0s3' (945d62c8-d2ed-3c81-be9d-9db3a1708d8f) successfully deleted. 

Connection 'enp0s3' (3c36b8c2-334b-57c7-91b6-4401f3489c69) successfully deleted. 
``` 

 
```bash 
[user@node1 ~]$ ip a 

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000 

    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00 

    inet 127.0.0.1/8 scope host lo 

       valid_lft forever preferred_lft forever 

    inet6 ::1/128 scope host 

       valid_lft forever preferred_lft forever 

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:41:bd:c2 brd ff:ff:ff:ff:ff:ff 

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:13:ed:18 brd ff:ff:ff:ff:ff:ff 

    inet 192.168.214.8/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8 

       valid_lft 342sec preferred_lft 342sec 

    inet6 fe80::301f:cd97:b9cb:1380/64 scope link noprefixroute 

       valid_lft forever preferred_lft forever 
``` 

 
```bash 

[user@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3 

``` 

 
```bash 
NAME=enp0s3 

DEVICE=enp0s3 

  

BOOTPROTO=dhcp 

ONBOOT=yes 

``` 

 
```bash 
[user@node1 ~]$ sudo nmcli con reload 

[user@node1 ~]$ sudo nmcli con up enp0s3 

Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/5) 
``` 

 
```bash 
[user@node1 ~]$ ip a 

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000 

    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00 

    inet 127.0.0.1/8 scope host lo 

       valid_lft forever preferred_lft forever 

    inet6 ::1/128 scope host 

       valid_lft forever preferred_lft forever 

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:41:bd:c2 brd ff:ff:ff:ff:ff:ff 

    inet 10.2.1.200/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3 

       valid_lft 593sec preferred_lft 593sec 

    inet6 fe80::a00:27ff:fe41:bdc2/64 scope link 

       valid_lft forever preferred_lft forever 

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 

    link/ether 08:00:27:13:ed:18 brd ff:ff:ff:ff:ff:ff 

    inet 192.168.214.8/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8 

       valid_lft 538sec preferred_lft 538sec 

    inet6 fe80::301f:cd97:b9cb:1380/64 scope link noprefixroute 

       valid_lft forever preferred_lft forever 

``` 

 
```bash 
[user@node1 ~]$ ip r s 

default via 10.2.1.254 dev enp0s3 proto dhcp src 10.2.1.200 metric 102 

10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.200 metric 102 

192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.8 metric 101 
``` 

 
```bash 
[user@node1 ~]$ ping 8.8.8.8 

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data. 

64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=51.7 ms 

64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=125 ms 

64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=83.0 ms 

^C 

--- 8.8.8.8 ping statistics --- 

3 packets transmitted, 3 received, 0% packet loss, time 2006ms 

rtt min/avg/max/mdev = 51.685/86.424/124.563/29.849 ms 

```

🌟 **BONUS**

- ajouter une autre ligne dans la conf du serveur DHCP pour qu'il donne aussi l'adresse d'un serveur DNS (utilisez `1.1.1.1` comme serveur DNS : c'est l'un des serveurs DNS de CloudFlare, un gros acteur du web)

☀️ **Wireshark it !**

- je veux une capture Wireshark qui contient l'échange DHCP DORA
- vous hébergerez la capture dans le dépôt Git avec le TP

> Si vous fouillez un peu dans l'échange DORA? vous pourrez voir les infos DHCP circuler : comme votre option DHCP qui a un champ dédié dans l'un des messages.

➜ A la fin de cette section vous avez donc :

- un serveur DHCP qui donne aux clients toutes les infos nécessaires pour avoir un accès internet automatique

# III. ARP

## 1. Les tables ARP

ARP est un protocole qui permet d'obtenir la MAC de quelqu'un, quand on connaît son IP.

On connaît toujours l'IP du correspondant avant de le joindre, c'est un prérequis. Quand vous tapez `ping 10.2.1.1`, vous connaissez l'IP, puisque vous venez de la taper :D

La machine va alors automatiquement effectuer un échange ARP sur le réseau, afin d'obtenir l'adresse MAC qui correspond à `10.2.1.1`.

Une fois l'info obtenue, l'info "telle IP correspond à telle MAC" est stockée dans la **table ARP**.

> Pour toutes les manips qui suivent, référez-vous au [mémo réseau Rocky](../../memo/rocky_network.md).

**Affichez la table ARP de `router.tp2.efrei`**

```bash
[user@router ~]$ ip neigh show 

192.168.214.1 dev enp0s3 lladdr 0a:00:27:00:00:0f REACHABLE 

10.2.1.200 dev enp0s9 lladdr 08:00:27:41:bd:c2 REACHABLE            =============   node1 

192.168.214.2 dev enp0s3 lladdr 08:00:27:0a:2c:f6 REACHABLE 

10.2.1.253 dev enp0s9 lladdr 08:00:27:15:a4:0a STALE    =========     dhcp 

192.168.171.254 dev enp0s8 lladdr 00:50:56:e4:11:a1 STALE    

10.2.1.1 dev enp0s9 lladdr 08:00:27:41:bd:c2 STALE 

192.168.171.2 dev enp0s8 lladdr 00:50:56:e9:6a:df REACHABLE 
```

**Capturez l'échange ARP avec Wireshark**

- je veux une capture de l'échange ARP livrée dans le dépôt Git
- l'échange ARP, c'est deux messages seulement : un ARP request et un ARP reply

## 2. ARP poisoning

Avant:

```bash
[user@node1 ~]$ ip neigh show
10.2.1.254 dev enp0s3 lladdr 08:00:27:cf:56:91 REACHABLE
10.2.1.253 dev enp0s3 lladdr 08:00:27:15:a4:0a STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:bb:7d:a4 STALE
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
[user@node1 ~]$
```

Autre PC

```bash
3: enpos8: «BROADCAST, MULTICAST, UP, LOWER_UP> mtu 1500 qdisc fq_codel state UP gr oup default qlen 1000
link/ether 08:00:27:21:41:2f brd ff:ff:ff:ff:ff:ff
inet 10.2.1.201/24 brd 10.2.1.255 scope global dynamic noprefixroute enpos8
valid lft 529sec preferred lft 529sec inet6 fe80::adf1:82:6dac: 9546/64 scope link noprefixroute
valid_lft forever preferred_lft forever

yukt@yuki-VirtualBox:$ sudo arping - c2 -s 08:00:27:21:41:2f -S 10.2.1.254 -p 10.2.1.201
[sudo] Mot de passe de yuki
ARPING 10.2.1.201
Timeout
Timeout
- - 10.2.1.201 statistics
2 packets transmitted, 0 packets received, 100% unanswered (0 extra)
```

Après

```bash
[user@node1 ~]$ ip neigh show
10.2.1.201 dev enp0s3 lladdr 08:00:27:21:41:2f STALE
10.2.1.254 dev enp0s3 lladdr 08:00:27:21:41:2f STALE
10.2.1.253 dev enp0s3 lladdr 08:00:27:15:a4:0a STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:bb:7d:a4 STALE
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
```



