# TP1 : Containers

Dans ce TP on va aborder plusieurs points autour de la conteneurisation : 

- Docker et son empreinte sur le système
- Manipulation d'images
- `docker-compose`

![Headaches](./img/headaches.jpg)

# Sommaire

- [TP1 : Containers](#tp1--containers)
- [Sommaire](#sommaire)
- [I. Docker](#i-docker)
  - [1. Install](#1-install)
  - [2. Construisez votre propre Dockerfile](#2-construisez-votre-propre-dockerfile)
- [III. `docker-compose`](#iii-docker-compose)
- [IV. Docker security](#iv-docker-security)
  - [2. Scan de vuln](#2-scan-de-vuln)



# I. Docker

## 1. Install

🌞 **Installer Docker sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/engine/install/)

```bash
[user@localhost ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
[sudo] password for user:
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[user@localhost ~]$ sudo dnf -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
......
```

- démarrer le service `docker` avec une commande `systemctl`
```bash
[user@localhost ~]$ sudo systemctl --now enable docker
[sudo] password for user:
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
```
- ajouter votre utilisateur au groupe `docker`
```bash
[user@localhost ~]$ sudo usermod -aG docker $(whoami)
```


🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`
  - l'app NGINX doit avoir un fichier de conf personnalisé
  - l'app NGINX doit servir un fichier `index.html` personnalisé
  - l'application doit être joignable grâce à un partage de ports
  - vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
  - le conteneur devra avoir un nom
  
```bash
[user@localhost ~]$ docker run --name web -d -v $(pwd)/nginx.conf:/etc/nginx/conf.d -v $(pwd):/var/www/tp_docker/ -p 8888:9999 nginx
873cb36910c247df12bc017b9f2c78dd0ce107e47632178af46343caebbfb090

```


## 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

- image de base (celle que vous voulez : debian, alpine, ubuntu, etc.)
  - une image du Docker Hub
  - qui ne porte aucune application par défaut
- vous ajouterez
  - mise à jour du système
  - installation de Apache (pour les systèmes debian, le serveur Web apache s'appelle `apache2` et non pas `httpd` comme sur Rocky)
  - page d'accueil Apache HTML personnalisée
```bash
[user@localhost DockerFile]$ cat apache2.conf

Listen 80


LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

DirectoryIndex index.html

DocumentRoot "/var/www/html/"

ErrorLog "logs/error.log"
LogLevel warn

[user@localhost DockerFile]$ cat index.html
site

[user@localhost DockerFile]$ cat Dockerfile

FROM debian:latest

RUN apt-get update  -y

RUN apt-get install -y apache2

COPY apache2.conf /etc/apache2/apache2.conf

COPY index.html /var/www/html/

EXPOSE 80

RUN mkdir /etc/apache2/logs

CMD ["apache2", "-DFOREGROUND"]


[user@localhost DockerFile]$ docker build -t web .

[user@localhost DockerFile]$ docker images
REPOSITORY     TAG       IMAGE ID       CREATED          SIZE
web            latest    a8db1daa589c   3 minutes ago    253MB

[user@localhost DockerFile]$ docker run -p 8080:80 -d web
d93d10d3510df22236e18665f0c6572aaecde9e60b9c01746dda4a8ecc0c0d2c

[user@localhost DockerFile]$ curl localhost:8080
site
```

# III. `docker-compose`


🌞 **Installez un WikiJS** en utilisant Docker

- WikiJS a besoin d'une base de données pour fonctionner
- il faudra donc deux conteneurs : un pour WikiJS et un pour la base de données
- référez-vous à la doc officielle de WikiJS, c'est tout guidé

```bash 
[user@localhost ~]$ nano docker-compose.yml
[user@localhost ~]$ cat docker-compose.yml
version: "3"
services:

  db:
    image: postgres:15-alpine
    environment:
      POSTGRES_DB: wiki
      POSTGRES_PASSWORD: wikijsrocks
      POSTGRES_USER: wikijs
    logging:
      driver: "none"
    restart: unless-stopped
    volumes:
      - db-data:/var/lib/postgresql/data

  wiki:
    image: ghcr.io/requarks/wiki:2
    depends_on:
      - db
    environment:
      DB_TYPE: postgres
      DB_HOST: db
      DB_PORT: 5432
      DB_USER: wikijs
      DB_PASS: wikijsrocks
      DB_NAME: wiki
    restart: unless-stopped
    ports:
      - "80:3000"

volumes:
  db-data:
[+] Running 22/23]$ docker compose up
[+] Running 22/23 [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulling                                                        249.5s
[+] Running 22/23 [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulling                                                        250.1s
[+] Running 23/23 [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulling                                                        250.8s
 ✔ wiki 13 layers [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                         264.5s
   ✔ 31e352740f53 Pull complete                                                                                    4.6s
   ✔ 2629b68d4311 Pull complete                                                                                  134.5s
   ✔ ddb7cc70f260 Pull complete                                                                                    5.7s
   ✔ 18afe6373474 Pull complete                                                                                    5.2s
   ✔ 88ec59e3b80e Pull complete                                                                                   29.1s
   ✔ 4f4fb700ef54 Pull complete                                                                                    6.2s
   ✔ 710fc81a2a82 Pull complete                                                                                   22.8s
   ✔ 4e75fc591abd Pull complete                                                                                  230.5s
   ✔ d11f75e9538b Pull complete                                                                                   29.7s
   ✔ 37595d564d08 Pull complete                                                                                   30.1s
   ✔ b6015734afb2 Pull complete                                                                                   30.6s
   ✔ 6c63ee65d20b Pull complete                                                                                   31.3s
   ✔ 2442912e7d55 Pull complete                                                                                   31.8s
 ✔ db 8 layers [⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                                  72.5s
   ✔ 96526aa774ef Pull complete                                                                                   33.0s
   ✔ f3836ccc9a67 Pull complete                                                                                   33.6s
   ✔ 3625732fb368 Pull complete                                                                                   34.1s
   ✔ db2cf8153bfe Pull complete                                                                                   54.1s
   ✔ 57d1e4d158ff Pull complete                                                                                   54.7s
   ✔ d4b037398667 Pull complete                                                                                   55.3s
   ✔ fffdf7fd1f65 Pull complete                                                                                   55.9s
   ✔ 6fbe9502899e Pull complete                                                                                   56.6s
[+] Running 4/4
 ✔ Network user_default   Created                                                                                  1.8s
 ✔ Volume "user_db-data"  Created                                                                                  0.0s
 ✔ Container user-db-1    Created                                                                                  2.3s
 ✔ Container user-wiki-1  Created                                                                                  0.1s
Attaching to user-db-1, user-wiki-1
user-db-1    | The files belonging to this database system will be owned by user "postgres".
user-db-1    | This user must also own the server process.
user-db-1    |
user-db-1    | The database cluster will be initialized with locale "en_US.utf8".
user-db-1    | The default database encoding has accordingly been set to "UTF8".
user-db-1    | The default text search configuration will be set to "english".
user-db-1    |
user-db-1    | Data page checksums are disabled.
user-db-1    |
user-db-1    | fixing permissions on existing directory /var/lib/postgresql/data ... ok
user-db-1    | creating subdirectories ... ok
user-db-1    | selecting dynamic shared memory implementation ... posix
user-db-1    | selecting default max_connections ... 100
user-db-1    | selecting default shared_buffers ... 128MB
user-db-1    | selecting default time zone ... UTC
user-db-1    | creating configuration files ... ok
user-db-1    | running bootstrap script ... ok
user-db-1    | sh: locale: not found
user-db-1    | 2023-11-16 16:31:29.629 UTC [29] WARNING:  no usable system locales were found
user-wiki-1  | Loading configuration from /wiki/config.yml... OK
user-wiki-1  | 2023-11-16T16:31:30.481Z [MASTER] info: =======================================
user-wiki-1  | 2023-11-16T16:31:30.487Z [MASTER] info: = Wiki.js 2.5.300 =====================
user-wiki-1  | 2023-11-16T16:31:30.491Z [MASTER] info: =======================================
user-wiki-1  | 2023-11-16T16:31:30.493Z [MASTER] info: Initializing...
user-db-1    | performing post-bootstrap initialization ... ok
user-db-1    | syncing data to disk ... ok
user-db-1    |
user-db-1    | initdb: warning: enabling "trust" authentication for local connections
user-db-1    | initdb: hint: You can change this by editing pg_hba.conf or using the option -A, or --auth-local and --auth-host, the next time you run initdb.
user-db-1    |
user-db-1    | Success. You can now start the database server using:
user-db-1    |
user-db-1    |     pg_ctl -D /var/lib/postgresql/data -l logfile start
user-db-1    |
user-db-1    | waiting for server to start....2023-11-16 16:31:33.780 UTC [35] LOG:  starting PostgreSQL 15.5 on x86_64-pc-linux-musl, compiled by gcc (Alpine 12.2.1_git20220924-r10) 12.2.1 20220924, 64-bit
user-db-1    | 2023-11-16 16:31:33.795 UTC [35] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
user-db-1    | 2023-11-16 16:31:33.830 UTC [38] LOG:  database system was shut down at 2023-11-16 16:31:31 UTC
user-db-1    | 2023-11-16 16:31:33.845 UTC [35] LOG:  database system is ready to accept connections
user-db-1    |  done
user-db-1    | server started
user-db-1    | CREATE DATABASE
user-db-1    |
user-db-1    |
user-db-1    | /usr/local/bin/docker-entrypoint.sh: ignoring /docker-entrypoint-initdb.d/*
user-db-1    |
user-db-1    | 2023-11-16 16:31:34.405 UTC [35] LOG:  received fast shutdown request
user-db-1    | waiting for server to shut down...2023-11-16 16:31:34.420 UTC [35] LOG:  aborting any active transactions
user-db-1    | 2023-11-16 16:31:34.432 UTC [35] LOG:  background worker "logical replication launcher" (PID 41) exited with exit code 1
user-db-1    | 2023-11-16 16:31:34.436 UTC [36] LOG:  shutting down
user-db-1    | .2023-11-16 16:31:34.449 UTC [36] LOG:  checkpoint starting: shutdown immediate
user-db-1    | 2023-11-16 16:31:35.192 UTC [36] LOG:  checkpoint complete: wrote 918 buffers (5.6%); 0 WAL file(s) added, 0 removed, 0 recycled; write=0.198 s, sync=0.512 s, total=0.753 s; sync files=301, longest=0.103 s, average=0.002 s; distance=4222 kB, estimate=4222 kB
user-db-1    | 2023-11-16 16:31:35.200 UTC [35] LOG:  database system is shut down
user-db-1    |  done
user-db-1    | server stopped
user-db-1    |
user-db-1    | PostgreSQL init process complete; ready for start up.
user-db-1    |
user-db-1    | 2023-11-16 16:31:35.315 UTC [1] LOG:  starting PostgreSQL 15.5 on x86_64-pc-linux-musl, compiled by gcc (Alpine 12.2.1_git20220924-r10) 12.2.1 20220924, 64-bit
user-db-1    | 2023-11-16 16:31:35.321 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
user-db-1    | 2023-11-16 16:31:35.327 UTC [1] LOG:  listening on IPv6 address "::", port 5432
user-db-1    | 2023-11-16 16:31:35.352 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
user-db-1    | 2023-11-16 16:31:35.377 UTC [51] LOG:  database system was shut down at 2023-11-16 16:31:35 UTC
user-db-1    | 2023-11-16 16:31:35.394 UTC [1] LOG:  database system is ready to accept connections
user-wiki-1  | 2023-11-16T16:31:35.957Z [MASTER] info: Using database driver pg for postgres [ OK ]
user-wiki-1  | 2023-11-16T16:31:35.965Z [MASTER] info: Connecting to database...
user-wiki-1  | 2023-11-16T16:31:36.039Z [MASTER] info: Database Connection Successful [ OK ]
user-wiki-1  | 2023-11-16T16:31:37.010Z [MASTER] warn: DB Configuration is empty or incomplete. Switching to Setup mode...
user-wiki-1  | 2023-11-16T16:31:37.013Z [MASTER] info: Starting setup wizard...
user-wiki-1  | 2023-11-16T16:31:37.484Z [MASTER] info: Starting HTTP server on port 3000...
user-wiki-1  | 2023-11-16T16:31:37.489Z [MASTER] info: HTTP Server on port: [ 3000 ]
user-wiki-1  | 2023-11-16T16:31:37.498Z [MASTER] info: HTTP Server: [ RUNNING ]
user-wiki-1  | 2023-11-16T16:31:37.500Z [MASTER] info: 🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻
user-wiki-1  | 2023-11-16T16:31:37.504Z [MASTER] info:
user-wiki-1  | 2023-11-16T16:31:37.506Z [MASTER] info: Browse to http://YOUR-SERVER-IP:3000/ to complete setup!
user-wiki-1  | 2023-11-16T16:31:37.509Z [MASTER] info:
user-wiki-1  | 2023-11-16T16:31:37.510Z [MASTER] info: 🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺


[user@localhost ~]$ docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED          STATUS          PORTS                                             NAMES
6dbe888caaf2   ghcr.io/requarks/wiki:2   "docker-entrypoint.s…"   7 minutes ago    Up 15 seconds   3443/tcp, 0.0.0.0:80->3000/tcp, :::80->3000/tcp                                          user-wiki-1
fcd47beea0a2   postgres:15-alpine        "docker-entrypoint.s…"   7 minutes ago    Up 16 seconds   5432/tcp                                          user-db-1
```


🌞 **Conteneurisez votre application**

- créer un `Dockerfile` maison qui porte l'application
- créer un `docker-compose.yml` qui permet de lancer votre application
- vous préciserez dans le rendu les instructions pour lancer l'application
  - indiquer la commande `git clone` pour récupérer votre dépôt
  - le `cd` dans le bon dossier
  - la commande `docker build` pour build l'image
  - la commande `docker-compose` pour lancer le(s) conteneur(s)
  - comme un vrai README qui m'explique comment lancer votre app !

📁 📁 `app/Dockerfile` et `app/docker-compose.yml`. Je veux un sous-dossier `app/` sur votre dépôt git avec ces deux fichiers dedans :)



```bash
[user@localhost appli]$ cat app/app.py
import redis
import time
import socket

time.sleep(5)

r = redis.StrictRedis(host='db', port=6379, db=0)

from flask import Flask, request, render_template
app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    hostname=socket.gethostname()
    return render_template('index.html',
                           title='Home',
                           container_hostname=hostname)

@app.route('/add', methods=['POST', 'GET'])
def add():
    if request.method == 'POST':
        r.set(request.form['key'], request.form['value'])

    return 'Successfully added key ' + request.form['key']

@app.route('/get', methods=['POST'])
def get():
    try:
        if request.method == 'POST':
            keyBytes = r.get(request.form['key'])
            key = keyBytes.decode('utf-8')
        return 'You asked about key ' + request.form['key'] + ". Value : " + key
    except:
        return 'Key ' + request.form['key'] + " does not exist."


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888)

[user@localhost app]$ cat requirements.txt
redis
flask

[user@localhost appli]$ cat docker-compose.yml
version: '3'

services:
  app:
    build: .
    ports:
      - "8888:8888"
    depends_on:
      - db
    networks:
      - my_network

  db:
    image: "redis:latest"
    ports:
      - "6379:6379"
    networks:
      - my_network
    container_name: db

networks:
  my_network:

[user@localhost appli]$ cat Dockerfile

FROM python:3

COPY . /app
COPY app/ /app

WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 8888

CMD ["python", "app.py"]


[user@localhost appli]$ cat templates/index.html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ title }}</title>
</head>
<body>
<h1>Add key</h1>
<form action="{{ url_for('add') }}" method = "POST">

Key:
<input type="text" name="key" >

Value:
<input type="text" name="value" >

<input type="submit" value="Submit">
</form>

<h1>Check key</h1>
<form action="{{ url_for('get') }}" method = "POST">

Key:
<input type="text" name="key" >
<input type="submit" value="Submit">
</form>

</body>
</html>
[user@localhost appli]$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                                       NAMES
4369e2d6ca76   appli-app      "python app.py"          3 minutes ago   Up 3 minutes   0.0.0.0:8888->8888/tcp, :::8888->8888/tcp   appli-app-1
82a6adb1313c   redis:latest   "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp   db

[user@localhost appli]$ curl http://172.20.10.3:8888
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>
<h1>Add key</h1>
<form action="/add" method = "POST">

Key:
<input type="text" name="key" >

Value:
<input type="text" name="value" >

<input type="submit" value="Submit">
</form>

<h1>Check key</h1>
<form action="/get" method = "POST">

Key:
<input type="text" name="key" >
<input type="submit" value="Submit">
</form>

</body>
```

# IV. Docker security


🌞 **Prouvez que vous pouvez devenir `root`**

- en étant membre du groupe `docker`
- sans taper aucune commande `sudo` ou `su` ou ce genre de choses
- normalement, une seule commande `docker run` suffit
- pour prouver que vous êtes `root`, plein de moyens possibles
  - par exemple un `cat /etc/shadow` qui contient les hash des mots de passe de la machine hôte
```bash
[user@localhost ~]$ cat /etc/sudoers
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
##
## This file must be edited with the 'visudo' command.
```
## 2. Scan de vuln

Il existe des outils dédiés au scan de vulnérabilités dans des images Docker.

C'est le cas de [Trivy](https://github.com/aquasecurity/trivy) par exemple.

🌞 **Utilisez Trivy**

- effectuez un scan de vulnérabilités sur des images précédemment mises en oeuvre :
  - celle de WikiJS que vous avez build

```bash 

[user@localhost ~]$ trivy image appli-app

C'est trop long donc j'en met une partie

│                              │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2021-31879                   │
├──────────────────────────────┼─────────────────────┼──────────┼──────────────┼─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ zlib1g                       │ CVE-2023-45853      │ CRITICAL │ will_not_fix │ 1:1.2.13.dfsg-1         │               │ zlib: integer overflow and resultant heap-based buffer       │
│                              │                     │          │              │                         │               │ overflow in zipOpenNewFileInZip4_6                           │
│                              │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-45853                   │
├──────────────────────────────┤                     │          │              │                         ├───────────────┤                                                              │
│ zlib1g-dev                   │                     │          │              │                         │               │                                                              │
│                              │                     │          │              │                         │               │                                                              │
│                              │                     │          │              │                         │               │                                                              │
└──────────────────────────────┴─────────────────────┴──────────┴──────────────┴─────────────────────────┴───────────────┴──────────────────────────────────────────────────────────────┘
2023-11-25T20:43:59.117+0100    INFO    Table result includes only package filenames. Use '--format json' option to get the full path to the package file.

Python (python-pkg)

Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 1, HIGH: 0, CRITICAL: 0)

┌────────────────┬───────────────┬──────────┬────────┬───────────────────┬───────────────┬──────────────────────────────────────────────────────────┐
│    Library     │ Vulnerability │ Severity │ Status │ Installed Version │ Fixed Version │                          Title                           │
├────────────────┼───────────────┼──────────┼────────┼───────────────────┼───────────────┼──────────────────────────────────────────────────────────┤
│ pip (METADATA) │ CVE-2023-5752 │ MEDIUM   │ fixed  │ 23.2.1            │ 23.3          │ pip: Mercurial configuration injectable in repo revision │
│                │               │          │        │                   │               │ when installing via pip                                  │
│                │               │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-5752                │
└────────────────┴───────────────┴──────────┴────────┴───────────────────┴───────────────┴──────────────────────────────────────────────────────────┘

```
  - celle de sa base de données
```bash
[user@localhost ~]$ trivy image redis:latest
2023-11-25T20:41:07.509+0100    INFO    Vulnerability scanning is enabled
2023-11-25T20:41:07.513+0100    INFO    Secret scanning is enabled
2023-11-25T20:41:07.517+0100    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-11-25T20:41:07.518+0100    INFO    Please see also https://aquasecurity.github.io/trivy/v0.47/docs/scanner/secret/#recommendation for faster secret detection

2023-11-25T20:41:30.086+0100    INFO    Detected OS: debian
2023-11-25T20:41:30.091+0100    INFO    Detecting Debian vulnerabilities...
2023-11-25T20:41:30.166+0100    INFO    Number of language-specific files: 1
2023-11-25T20:41:30.174+0100    INFO    Detecting gobinary vulnerabilities...

redis:latest (debian 12.2)

Total: 184 (UNKNOWN: 0, LOW: 136, MEDIUM: 33, HIGH: 14, CRITICAL: 1)


```
  - l'image de Apache que vous avez build
```bash
[user@localhost ~]$ trivy image ghcr.io/requarks/wiki:2
2023-11-25T20:19:02.880+0100    INFO    Vulnerability scanning is enabled
2023-11-25T20:19:02.884+0100    INFO    Secret scanning is enabled
2023-11-25T20:19:02.888+0100    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-11-25T20:19:02.888+0100    INFO    Please see also https://aquasecurity.github.io/trivy/v0.47/docs/scanner/secret/#recommendation for faster secret detection
2023-11-25T20:21:27.715+0100    INFO    Detected OS: alpine
2023-11-25T20:21:27.733+0100    INFO    Detecting Alpine vulnerabilities...
2023-11-25T20:21:27.836+0100    INFO    Number of language-specific files: 1
2023-11-25T20:21:27.840+0100    INFO    Detecting node-pkg vulnerabilities...

ghcr.io/requarks/wiki:2 (alpine 3.18.2)

Total: 20 (UNKNOWN: 0, LOW: 2, MEDIUM: 8, HIGH: 5, CRITICAL: 5)

┌───────────────┬────────────────┬──────────┬────────┬───────────────────┬───────────────┬─────────────────────────────────────────────────────────────┐
│    Library    │ Vulnerability  │ Severity │ Status │ Installed Version │ Fixed Version │                            Title                            │
├───────────────┼────────────────┼──────────┼────────┼───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ busybox       │ CVE-2022-48174 │ CRITICAL │ fixed  │ 1.36.1-r0         │ 1.36.1-r1     │ stack overflow vulnerability in ash.c leads to arbitrary    │
│               │                │          │        │                   │               │ code execution                                              │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2022-48174                  │
├───────────────┤                │          │        │                   │               │                                                             │
│ busybox-binsh │                │          │        │                   │               │                                                             │
│               │                │          │        │                   │               │                                                             │
│               │                │          │        │                   │               │                                                             │
├───────────────┼────────────────┤          │        ├───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ curl          │ CVE-2023-38545 │          │        │ 8.2.1-r0          │ 8.4.0-r0      │ curl: heap based buffer overflow in the SOCKS5 proxy        │
│               │                │          │        │                   │               │ handshake                                                   │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-38545                  │
│               ├────────────────┼──────────┤        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-38039 │ HIGH     │        │                   │ 8.3.0-r0      │ curl: out of heap memory issue due to missing limit on      │
│               │                │          │        │                   │               │ header...                                                   │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-38039                  │
│               ├────────────────┼──────────┤        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-38546 │ LOW      │        │                   │ 8.4.0-r0      │ curl: cookie injection with none file                       │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-38546                  │
├───────────────┼────────────────┼──────────┤        ├───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ libcrypto3    │ CVE-2023-5363  │ HIGH     │        │ 3.1.1-r1          │ 3.1.4-r0      │ openssl: Incorrect cipher key and IV length processing      │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-5363                   │
│               ├────────────────┼──────────┤        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-2975  │ MEDIUM   │        │                   │ 3.1.1-r2      │ openssl: AES-SIV cipher implementation contains a bug that  │
│               │                │          │        │                   │               │ causes it to ignore...                                      │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-2975                   │
│               ├────────────────┤          │        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-3446  │          │        │                   │ 3.1.1-r3      │ openssl: Excessive time spent checking DH keys and          │
│               │                │          │        │                   │               │ parameters                                                  │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-3446                   │
│               ├────────────────┤          │        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-3817  │          │        │                   │ 3.1.2-r0      │ OpenSSL: Excessive time spent checking DH q parameter value │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-3817                   │
│               ├────────────────┤          │        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-5678  │          │        │                   │ 3.1.4-r1      │ openssl: Generating excessively long X9.42 DH keys or       │
│               │                │          │        │                   │               │ checking excessively long X9.42...                          │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-5678                   │
├───────────────┼────────────────┼──────────┤        ├───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ libcurl       │ CVE-2023-38545 │ CRITICAL │        │ 8.2.1-r0          │ 8.4.0-r0      │ curl: heap based buffer overflow in the SOCKS5 proxy        │
│               │                │          │        │                   │               │ handshake                                                   │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-38545                  │
│               ├────────────────┼──────────┤        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-38039 │ HIGH     │        │                   │ 8.3.0-r0      │ curl: out of heap memory issue due to missing limit on      │
│               │                │          │        │                   │               │ header...                                                   │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-38039                  │
│               ├────────────────┼──────────┤        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-38546 │ LOW      │        │                   │ 8.4.0-r0      │ curl: cookie injection with none file                       │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-38546                  │
├───────────────┼────────────────┼──────────┤        ├───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ libssl3       │ CVE-2023-5363  │ HIGH     │        │ 3.1.1-r1          │ 3.1.4-r0      │ openssl: Incorrect cipher key and IV length processing      │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-5363                   │
│               ├────────────────┼──────────┤        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-2975  │ MEDIUM   │        │                   │ 3.1.1-r2      │ openssl: AES-SIV cipher implementation contains a bug that  │
│               │                │          │        │                   │               │ causes it to ignore...                                      │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-2975                   │
│               ├────────────────┤          │        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-3446  │          │        │                   │ 3.1.1-r3      │ openssl: Excessive time spent checking DH keys and          │
│               │                │          │        │                   │               │ parameters                                                  │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-3446                   │
│               ├────────────────┤          │        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-3817  │          │        │                   │ 3.1.2-r0      │ OpenSSL: Excessive time spent checking DH q parameter value │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-3817                   │
│               ├────────────────┤          │        │                   ├───────────────┼─────────────────────────────────────────────────────────────┤
│               │ CVE-2023-5678  │          │        │                   │ 3.1.4-r1      │ openssl: Generating excessively long X9.42 DH keys or       │
│               │                │          │        │                   │               │ checking excessively long X9.42...                          │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-5678                   │
├───────────────┼────────────────┼──────────┤        ├───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ nghttp2-libs  │ CVE-2023-44487 │ HIGH     │        │ 1.55.1-r0         │ 1.57.0-r0     │ HTTP/2: Multiple HTTP/2 enabled web servers are vulnerable  │
│               │                │          │        │                   │               │ to a DDoS attack...                                         │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2023-44487                  │
├───────────────┼────────────────┼──────────┤        ├───────────────────┼───────────────┼─────────────────────────────────────────────────────────────┤
│ ssl_client    │ CVE-2022-48174 │ CRITICAL │        │ 1.36.1-r0         │ 1.36.1-r1     │ stack overflow vulnerability in ash.c leads to arbitrary    │
│               │                │          │        │                   │               │ code execution                                              │
│               │                │          │        │                   │               │ https://avd.aquasec.com/nvd/cve-2022-48174                  │
└───────────────┴────────────────┴──────────┴────────┴───────────────────┴───────────────┴─────────────────────────────────────────────────────────────┘
2023-11-25T20:21:28.283+0100    INFO    Table result includes only package filenames. Use '--format json' option to get the full path to the package file.

Node.js (node-pkg)

Total: 46 (UNKNOWN: 0, LOW: 1, MEDIUM: 28, HIGH: 13, CRITICAL: 4)

┌─────────────────────────────────────┬─────────────────────┬──────────┬──────────┬───────────────────┬────────────────────────────┬──────────────────────────────────────────────────────────────┐
│               Library               │    Vulnerability    │ Severity │  Status  │ Installed Version │       Fixed Version        │                            Title                             │
├─────────────────────────────────────┼─────────────────────┼──────────┼──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ @babel/traverse (package.json)      │ CVE-2023-45133      │ CRITICAL │ fixed    │ 7.12.13           │ 7.23.2, 8.0.0-alpha.4      │ arbitrary code execution                                     │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2023-45133                   │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 7.22.10           │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ apollo-server (package.json)        │ GHSA-qm7x-rc44-rrqw │ HIGH     │          │ 2.25.2            │ 2.25.3, 3.4.1              │ Cross-site Scripting Vulnerability in GraphQL Playground     │
│                                     │                     │          │          │                   │                            │ (distributed by Apollo Server)                               │
│                                     │                     │          │          │                   │                            │ https://github.com/advisories/GHSA-qm7x-rc44-rrqw            │
│                                     ├─────────────────────┼──────────┤          │                   ├────────────────────────────┼──────────────────────────────────────────────────────────────┤
│                                     │ GHSA-2p3c-p3qw-69r4 │ MEDIUM   │          │                   │ 2.25.4                     │ The graphql-upload library included in Apollo Server 2 is    │
│                                     │                     │          │          │                   │                            │ vulnerable to CSRF...                                        │
│                                     │                     │          │          │                   │                            │ https://github.com/advisories/GHSA-2p3c-p3qw-69r4            │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ axios (package.json)                │ CVE-2023-45857      │          │          │ 0.21.4            │ 1.6.0                      │ axios: exposure of confidential data stored in cookies       │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2023-45857                   │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 0.27.2            │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ cross-fetch (package.json)          │ CVE-2022-1365       │          │          │ 1.1.1             │ 3.1.5, 2.2.6               │ Exposure of Private Personal Information to an Unauthorized  │
│                                     │                     │          │          │                   │                            │ Actor                                                        │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-1365                    │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 3.0.6             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 3.1.4             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ css-what (package.json)             │ CVE-2021-33587      │ HIGH     │          │ 4.0.0             │ 5.0.1                      │ does not ensure that attribute parsing has linear time       │
│                                     │                     │          │          │                   │                            │ complexity relative to...                                    │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2021-33587                   │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ debug (package.json)                │ CVE-2017-16137      │ MEDIUM   │          │ 4.1.1             │ 2.6.9, 3.1.0, 3.2.7, 4.3.1 │ nodejs-debug: Regular expression Denial of Service           │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2017-16137                   │
├─────────────────────────────────────┼─────────────────────┼──────────┼──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ dicer (package.json)                │ CVE-2022-24434      │ HIGH     │ affected │ 0.2.5             │                            │ nodejs service crash by sending a crafted payload            │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-24434                   │
│                                     │                     │          │          ├───────────────────┼────────────────────────────┤                                                              │
│                                     │                     │          │          │ 0.3.0             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ express-brute (package.json)        │ GHSA-984p-xq9m-4rjw │ MEDIUM   │          │ 1.0.1             │                            │ Rate Limiting Bypass in express-brute                        │
│                                     │                     │          │          │                   │                            │ https://github.com/advisories/GHSA-984p-xq9m-4rjw            │
├─────────────────────────────────────┼─────────────────────┼──────────┼──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ file-type (package.json)            │ CVE-2022-36313      │ HIGH     │ fixed    │ 15.0.1            │ 16.5.4, 17.1.3             │ a malformed MKV file could cause the file type detector to   │
│                                     │                     │          │          │                   │                            │ get...                                                       │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-36313                   │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ highlight.js (package.json)         │ GHSA-7wwv-vh3v-89cq │ MEDIUM   │          │ 10.2.1            │ 10.4.1                     │ ReDOS vulnerabities: multiple grammars                       │
│                                     │                     │          │          │                   │                            │ https://github.com/advisories/GHSA-7wwv-vh3v-89cq            │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 10.3.1            │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ jsdom (package.json)                │ CVE-2021-20066      │          │          │ 16.4.0            │ 16.5.0                     │ improper loading of local resources                          │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2021-20066                   │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ json5 (package.json)                │ CVE-2022-46175      │ HIGH     │          │ 2.0.0             │ 2.2.2, 1.0.2               │ json5: Prototype Pollution in JSON5 via Parse Method         │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-46175                   │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ knex (package.json)                 │ CVE-2016-20018      │          │          │ 0.21.21           │ 2.4.0                      │ Knex.js has a limited SQL injection vulnerability            │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2016-20018                   │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 0.21.7            │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ luxon (package.json)                │ CVE-2023-22467      │          │          │ 1.25.0            │ 1.28.1, 2.5.2, 3.2.1       │ luxon: Inefficient regular expression complexity in luxon.js │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2023-22467                   │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ markdown-it (package.json)          │ CVE-2022-21670      │ MEDIUM   │          │ 11.0.1            │ 12.3.2                     │ markdown-it is a Markdown parser. Prior to version 1.3.2,    │
│                                     │                     │          │          │                   │                            │ special patt ......                                          │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-21670                   │
├─────────────────────────────────────┼─────────────────────┤          ├──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ markdown-it-decorate (package.json) │ CVE-2020-28459      │          │ affected │ 1.2.2             │                            │ markdown-it-decorate vulnerable to cross-site scripting      │
│                                     │                     │          │          │                   │                            │ (XSS)                                                        │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2020-28459                   │
├─────────────────────────────────────┼─────────────────────┤          ├──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ mongodb (package.json)              │ CVE-2021-32050      │          │ fixed    │ 3.6.5             │ 3.6.10, 4.17.0, 5.8.0      │ Some MongoDB Drivers may erroneously publish events          │
│                                     │                     │          │          │                   │                            │ containing authent ...                                       │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2021-32050                   │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ node-fetch (package.json)           │ CVE-2022-0235       │ HIGH     │          │ 1.7.3             │ 3.1.1, 2.6.7               │ node-fetch: exposure of sensitive information to an          │
│                                     │                     │          │          │                   │                            │ unauthorized actor                                           │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-0235                    │
│                                     ├─────────────────────┼──────────┤          │                   ├────────────────────────────┼──────────────────────────────────────────────────────────────┤
│                                     │ CVE-2020-15168      │ LOW      │          │                   │ 2.6.1, 3.0.0-beta.9        │ size of data after fetch() JS thread leads to DoS            │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2020-15168                   │
│                                     ├─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│                                     │ CVE-2022-0235       │ HIGH     │          │ 2.6.1             │ 3.1.1, 2.6.7               │ node-fetch: exposure of sensitive information to an          │
│                                     │                     │          │          │                   │                            │ unauthorized actor                                           │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-0235                    │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ node-uuid (package.json)            │ CVE-2015-8851       │ MEDIUM   │          │ 1.4.1             │ >=1.4.4                    │ nodejs-node-uuid: insecure entropy source - Math.random()    │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2015-8851                    │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ passport (package.json)             │ CVE-2022-25896      │          │          │ 0.4.1             │ 0.6.0                      │ passport: incorrect session regeneration                     │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-25896                   │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ passport-oauth2 (package.json)      │ CVE-2021-41580      │          │          │ 1.2.0             │ 1.6.1                      │ Improper Access Control in passport-oauth2                   │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2021-41580                   │
├─────────────────────────────────────┼─────────────────────┤          ├──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ request (package.json)              │ CVE-2023-28155      │          │ affected │ 2.88.2            │                            │ The Request package through 2.88.1 for Node.js allows a      │
│                                     │                     │          │          │                   │                            │ bypass of SSRF...                                            │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2023-28155                   │
├─────────────────────────────────────┼─────────────────────┤          ├──────────┼───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ semver (package.json)               │ CVE-2022-25883      │          │ fixed    │ 4.3.2             │ 7.5.2, 6.3.1, 5.7.2        │ nodejs-semver: Regular expression denial of service          │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-25883                   │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 7.3.8             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 7.5.1             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ sqlite3 (package.json)              │ CVE-2022-43441      │ HIGH     │          │ 5.1.4             │ 5.1.5                      │ A code execution vulnerability exists in the Statement       │
│                                     │                     │          │          │                   │                            │ Bindings functi ...                                          │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2022-43441                   │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ tough-cookie (package.json)         │ CVE-2023-26136      │ MEDIUM   │          │ 2.5.0             │ 4.1.3                      │ tough-cookie: prototype pollution in cookie memstore         │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2023-26136                   │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 3.0.1             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ underscore (package.json)           │ CVE-2021-23358      │ CRITICAL │          │ 1.6.0             │ 1.12.1                     │ nodejs-underscore: Arbitrary code execution via the template │
│                                     │                     │          │          │                   │                            │ function                                                     │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2021-23358                   │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 1.8.3             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┼──────────┤          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ ws (package.json)                   │ CVE-2021-32640      │ MEDIUM   │          │ 7.4.5             │ 7.4.6, 6.2.2, 5.2.3        │ Specially crafted value of the `Sec-Websocket-Protocol`      │
│                                     │                     │          │          │                   │                            │ header can be used to significantly...                       │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2021-32640                   │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
├─────────────────────────────────────┼─────────────────────┤          │          ├───────────────────┼────────────────────────────┼──────────────────────────────────────────────────────────────┤
│ xml2js (package.json)               │ CVE-2023-0842       │          │          │ 0.4.19            │ 0.5.0                      │ node-xml2js: xml2js is vulnerable to prototype pollution     │
│                                     │                     │          │          │                   │                            │ https://avd.aquasec.com/nvd/cve-2023-0842                    │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 0.4.23            │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
│                                     │                     │          │          ├───────────────────┤                            │                                                              │
│                                     │                     │          │          │ 0.4.4             │                            │                                                              │
│                                     │                     │          │          │                   │                            │                                                              │
└─────────────────────────────────────┴─────────────────────┴──────────┴──────────┴───────────────────┴────────────────────────────┴──────────────────────────────────────────────────────────────┘
```
  - celle de sa base de données
```bash 
[user@localhost ~]$ trivy image postgres:15-alpine
2023-11-25T20:25:28.649+0100    INFO    Vulnerability scanning is enabled
2023-11-25T20:25:28.653+0100    INFO    Secret scanning is enabled
2023-11-25T20:25:28.658+0100    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-11-25T20:25:28.658+0100    INFO    Please see also https://aquasecurity.github.io/trivy/v0.47/docs/scanner/secret/#recommendation for faster secret detection
2023-11-25T20:25:28.726+0100    INFO    Detected OS: alpine
2023-11-25T20:25:28.732+0100    INFO    Detecting Alpine vulnerabilities...
2023-11-25T20:25:28.747+0100    INFO    Number of language-specific files: 0

postgres:15-alpine (alpine 3.18.4)

Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
```
  - l'image de NGINX officielle utilisée dans la première partie
```bash 
[user@localhost ~]$ trivy image nginx
2023-11-25T21:08:28.292+0100    INFO    Vulnerability scanning is enabled
2023-11-25T21:08:28.297+0100    INFO    Secret scanning is enabled
2023-11-25T21:08:28.301+0100    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-11-25T21:08:28.302+0100    INFO    Please see also https://aquasecurity.github.io/trivy/v0.47/docs/scanner/secret/#recommendation for faster secret detection
2023-11-25T21:08:52.072+0100    INFO    Detected OS: debian
2023-11-25T21:08:52.083+0100    INFO    Detecting Debian vulnerabilities...
2023-11-25T21:08:52.238+0100    INFO    Number of language-specific files: 0

nginx (debian 12.2)

Total: 113 (UNKNOWN: 2, LOW: 82, MEDIUM: 22, HIGH: 6, CRITICAL: 1)

┌──────────────────┬─────────────────────┬──────────┬──────────────┬─────────────────────────┬───────────────┬──────────────────────────────────────────────────────────────┐
│     Library      │    Vulnerability    │ Severity │    Status    │    Installed Version    │ Fixed Version │                            Title                             │
├──────────────────┼─────────────────────┼──────────┼──────────────┼─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ apt              │ CVE-2011-3374       │ LOW      │ affected     │ 2.6.1                   │               │ It was found that apt-key in apt, all versions, do not       │
│                  │                     │          │              │                         │               │ correctly...                                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2011-3374                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ bash             │ TEMP-0841856-B18BAF │          │              │ 5.2.15-2+b2             │               │ [Privilege escalation possible to other user than root]      │
│                  │                     │          │              │                         │               │ https://security-tracker.debian.org/tracker/TEMP-0841856-B1- │
│                  │                     │          │              │                         │               │ 8BAF                                                         │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ bsdutils         │ CVE-2022-0563       │          │              │ 1:2.38.1-5+b1           │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┼─────────────────────┤          ├──────────────┼─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ coreutils        │ CVE-2016-2781       │          │ will_not_fix │ 9.1-1                   │               │ coreutils: Non-privileged session can escape to the parent   │
│                  │                     │          │              │                         │               │ session in chroot                                            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2016-2781                    │
│                  ├─────────────────────┤          ├──────────────┤                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-18018      │          │ affected     │                         │               │ coreutils: race condition vulnerability in chown and chgrp   │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-18018                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ gcc-12-base      │ CVE-2023-4039       │ MEDIUM   │              │ 12.2.0-14               │               │ gcc: -fstack-protector fails to guard dynamic stack          │
│                  │                     │          │              │                         │               │ allocations on ARM64                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-4039                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2022-27943      │ LOW      │              │                         │               │ libiberty/rust-demangle.c in GNU GCC 11.2 allows stack       │
│                  │                     │          │              │                         │               │ exhaustion in demangle_const                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-27943                   │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ gpgv             │ CVE-2022-3219       │          │              │ 2.2.40-1.1              │               │ denial of service issue (resource consumption) using         │
│                  │                     │          │              │                         │               │ compressed packets                                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-3219                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libaom3          │ CVE-2023-39616      │ HIGH     │              │ 3.6.0-1                 │               │ AOMedia v3.0.0 to v3.5.0 was discovered to contain an        │
│                  │                     │          │              │                         │               │ invalid read mem...                                          │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-39616                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libapt-pkg6.0    │ CVE-2011-3374       │ LOW      │              │ 2.6.1                   │               │ It was found that apt-key in apt, all versions, do not       │
│                  │                     │          │              │                         │               │ correctly...                                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2011-3374                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libblkid1        │ CVE-2022-0563       │          │              │ 2.38.1-5+b1             │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libc-bin         │ CVE-2010-4756       │          │              │ 2.36-9+deb12u3          │               │ glibc: glob implementation can cause excessive CPU and       │
│                  │                     │          │              │                         │               │ memory consumption due to...                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2010-4756                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2018-20796      │          │              │                         │               │ glibc: uncontrolled recursion in function                    │
│                  │                     │          │              │                         │               │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2018-20796                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010022    │          │              │                         │               │ glibc: stack guard protection bypass                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010022                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010023    │          │              │                         │               │ glibc: running ldd on malicious ELF leads to code execution  │
│                  │                     │          │              │                         │               │ because of...                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010023                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010024    │          │              │                         │               │ glibc: ASLR bypass using cache of thread stack and heap      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010024                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010025    │          │              │                         │               │ glibc: information disclosure of heap addresses of           │
│                  │                     │          │              │                         │               │ pthread_created thread                                       │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010025                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-9192       │          │              │                         │               │ glibc: uncontrolled recursion in function                    │
│                  │                     │          │              │                         │               │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-9192                    │
├──────────────────┼─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│ libc6            │ CVE-2010-4756       │          │              │                         │               │ glibc: glob implementation can cause excessive CPU and       │
│                  │                     │          │              │                         │               │ memory consumption due to...                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2010-4756                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2018-20796      │          │              │                         │               │ glibc: uncontrolled recursion in function                    │
│                  │                     │          │              │                         │               │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2018-20796                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010022    │          │              │                         │               │ glibc: stack guard protection bypass                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010022                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010023    │          │              │                         │               │ glibc: running ldd on malicious ELF leads to code execution  │
│                  │                     │          │              │                         │               │ because of...                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010023                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010024    │          │              │                         │               │ glibc: ASLR bypass using cache of thread stack and heap      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010024                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-1010025    │          │              │                         │               │ glibc: information disclosure of heap addresses of           │
│                  │                     │          │              │                         │               │ pthread_created thread                                       │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-1010025                 │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-9192       │          │              │                         │               │ glibc: uncontrolled recursion in function                    │
│                  │                     │          │              │                         │               │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-9192                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libdav1d6        │ CVE-2023-32570      │ MEDIUM   │              │ 1.0.0-2                 │               │ VideoLAN dav1d before 1.2.0 has a thread_task.c race         │
│                  │                     │          │              │                         │               │ condition that ca ......                                     │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-32570                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libde265-0       │ CVE-2023-27103      │ HIGH     │              │ 1.0.11-1                │               │ Libde265 v1.0.11 was discovered to contain a heap buffer     │
│                  │                     │          │              │                         │               │ overflow via ...                                             │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-27103                   │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-27102      │ MEDIUM   │              │                         │               │ Libde265 v1.0.11 was discovered to contain a segmentation    │
│                  │                     │          │              │                         │               │ violation vi ...                                             │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-27102                   │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-43887      │ UNKNOWN  │              │                         │               │ Libde265 v1.0.12 was discovered to contain multiple buffer   │
│                  │                     │          │              │                         │               │ overflows v ...                                              │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-43887                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-47471      │          │              │                         │               │ Buffer Overflow vulnerability in strukturag libde265         │
│                  │                     │          │              │                         │               │ v1.10.12 allows a ...                                        │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-47471                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libgcc-s1        │ CVE-2023-4039       │ MEDIUM   │              │ 12.2.0-14               │               │ gcc: -fstack-protector fails to guard dynamic stack          │
│                  │                     │          │              │                         │               │ allocations on ARM64                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-4039                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2022-27943      │ LOW      │              │                         │               │ libiberty/rust-demangle.c in GNU GCC 11.2 allows stack       │
│                  │                     │          │              │                         │               │ exhaustion in demangle_const                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-27943                   │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libgcrypt20      │ CVE-2018-6829       │          │              │ 1.10.1-3                │               │ libgcrypt: ElGamal implementation doesn't have semantic      │
│                  │                     │          │              │                         │               │ security due to incorrectly encoded plaintexts...            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2018-6829                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libgnutls30      │ CVE-2023-5981       │ MEDIUM   │              │ 3.7.9-2                 │               │ [ttiming side-channel inside RSA-PSK key exchange]           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-5981                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2011-3389       │ LOW      │              │                         │               │ HTTPS: block-wise chosen-plaintext attack against SSL/TLS    │
│                  │                     │          │              │                         │               │ (BEAST)                                                      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2011-3389                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libgssapi-krb5-2 │ CVE-2018-5709       │          │              │ 1.20.1-2+deb12u1        │               │ krb5: integer overflow in dbentry->n_key_data in             │
│                  │                     │          │              │                         │               │ kadmin/dbutil/dump.c                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2018-5709                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libheif1         │ CVE-2023-29659      │ MEDIUM   │              │ 1.15.1-1                │               │ A Segmentation fault caused by a floating point exception    │
│                  │                     │          │              │                         │               │ exists in li...                                              │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-29659                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libjbig0         │ CVE-2017-9937       │ LOW      │              │ 2.1-6.1                 │               │ libtiff: memory malloc failure in tif_jbig.c could cause     │
│                  │                     │          │              │                         │               │ DOS.                                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-9937                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libk5crypto3     │ CVE-2018-5709       │          │              │ 1.20.1-2+deb12u1        │               │ krb5: integer overflow in dbentry->n_key_data in             │
│                  │                     │          │              │                         │               │ kadmin/dbutil/dump.c                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2018-5709                    │
├──────────────────┤                     │          │              │                         ├───────────────┤                                                              │
│ libkrb5-3        │                     │          │              │                         │               │                                                              │
│                  │                     │          │              │                         │               │                                                              │
│                  │                     │          │              │                         │               │                                                              │
├──────────────────┤                     │          │              │                         ├───────────────┤                                                              │
│ libkrb5support0  │                     │          │              │                         │               │                                                              │
│                  │                     │          │              │                         │               │                                                              │
│                  │                     │          │              │                         │               │                                                              │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libldap-2.5-0    │ CVE-2023-2953       │ HIGH     │              │ 2.5.13+dfsg-5           │               │ null pointer dereference in ber_memalloc_x function          │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-2953                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2015-3276       │ LOW      │              │                         │               │ incorrect multi-keyword mode cipherstring parsing            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2015-3276                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-14159      │          │              │                         │               │ openldap: Privilege escalation via PID file manipulation     │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-14159                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-17740      │          │              │                         │               │ openldap: contrib/slapd-modules/nops/nops.c attempts to free │
│                  │                     │          │              │                         │               │ stack buffer allowing remote attackers to cause...           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-17740                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2020-15719      │          │              │                         │               │ openldap: Certificate validation incorrectly matches name    │
│                  │                     │          │              │                         │               │ against CN-ID                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2020-15719                   │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libmount1        │ CVE-2022-0563       │          │              │ 2.38.1-5+b1             │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libnghttp2-14    │ CVE-2023-44487      │ HIGH     │              │ 1.52.0-1                │               │ HTTP/2: Multiple HTTP/2 enabled web servers are vulnerable   │
│                  │                     │          │              │                         │               │ to a DDoS attack...                                          │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-44487                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libpng16-16      │ CVE-2021-4214       │ LOW      │              │ 1.6.39-2                │               │ libpng: hardcoded value leads to heap-overflow               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2021-4214                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libsmartcols1    │ CVE-2022-0563       │          │              │ 2.38.1-5+b1             │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libssl3          │ CVE-2023-5678       │ MEDIUM   │              │ 3.0.11-1~deb12u2        │               │ openssl: Generating excessively long X9.42 DH keys or        │
│                  │                     │          │              │                         │               │ checking excessively long X9.42...                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-5678                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2007-6755       │ LOW      │              │                         │               │ Dual_EC_DRBG: weak pseudo random number generator            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2007-6755                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2010-0928       │          │              │                         │               │ openssl: RSA authentication weakness                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2010-0928                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libstdc++6       │ CVE-2023-4039       │ MEDIUM   │              │ 12.2.0-14               │               │ gcc: -fstack-protector fails to guard dynamic stack          │
│                  │                     │          │              │                         │               │ allocations on ARM64                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-4039                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2022-27943      │ LOW      │              │                         │               │ libiberty/rust-demangle.c in GNU GCC 11.2 allows stack       │
│                  │                     │          │              │                         │               │ exhaustion in demangle_const                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-27943                   │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libsystemd0      │ CVE-2013-4392       │          │              │ 252.17-1~deb12u1        │               │ TOCTOU race condition when updating file permissions and     │
│                  │                     │          │              │                         │               │ SELinux security contexts                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2013-4392                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31437      │          │              │                         │               │ An issue was discovered in systemd 253. An attacker can      │
│                  │                     │          │              │                         │               │ modify a...                                                  │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31437                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31438      │          │              │                         │               │ An issue was discovered in systemd 253. An attacker can      │
│                  │                     │          │              │                         │               │ truncate a...                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31438                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31439      │          │              │                         │               │ An issue was discovered in systemd 253. An attacker can      │
│                  │                     │          │              │                         │               │ modify the...                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31439                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libtiff6         │ CVE-2023-6277       │ HIGH     │              │ 4.5.0-6                 │               │ libtiff: Out-of-memory in TIFFOpen via a craft file          │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-6277                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-25433      │ MEDIUM   │              │                         │               │ libtiff: Buffer Overflow via /libtiff/tools/tiffcrop.c       │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-25433                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-26965      │          │              │                         │               │ libtiff: heap-based use after free via a crafted TIFF image  │
│                  │                     │          │              │                         │               │ in loadImage()...                                            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-26965                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-26966      │          │              │                         │               │ libtiff: Buffer Overflow in uv_encode()                      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-26966                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-2908       │          │              │                         │               │ libtiff: null pointer dereference in tif_dir.c               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-2908                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-3316       │          │              │                         │               │ libtiff: tiffcrop: null pointer dereference in TIFFClose()   │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-3316                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-3576       │          │              │                         │               │ memory leak in tiffcrop.c                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-3576                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-3618       │          │              │                         │               │ libtiff: segmentation fault in Fax3Encode in                 │
│                  │                     │          │              │                         │               │ libtiff/tif_fax3.c                                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-3618                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-40745      │          │              │                         │               │ libtiff: integer overflow in tiffcp.c                        │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-40745                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-41175      │          │              │                         │               │ libtiff: potential integer overflow in raw2tiff.c            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-41175                   │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-16232      │ LOW      │              │                         │               │ libtiff: Memory leaks in tif_open.c, tif_lzw.c, and          │
│                  │                     │          │              │                         │               │ tif_aux.c                                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-16232                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-17973      │          │              │                         │               │ libtiff: heap-based use after free in                        │
│                  │                     │          │              │                         │               │ tiff2pdf.c:t2p_writeproc                                     │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-17973                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-5563       │          │              │                         │               │ libtiff: Heap-buffer overflow in LZWEncode tif_lzw.c         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-5563                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2017-9117       │          │              │                         │               │ libtiff: Heap-based buffer over-read in bmp2tiff             │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2017-9117                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2018-10126      │          │              │                         │               │ libtiff: NULL pointer dereference in the jpeg_fdct_16x16     │
│                  │                     │          │              │                         │               │ function in jfdctint.c                                       │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2018-10126                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2022-1210       │          │              │                         │               │ tiff: Malicious file leads to a denial of service in TIFF    │
│                  │                     │          │              │                         │               │ File...                                                      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-1210                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-1916       │          │              │                         │               │ libtiff: out-of-bounds read in extractImageSection() in      │
│                  │                     │          │              │                         │               │ tools/tiffcrop.c                                             │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-1916                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-3164       │          │              │                         │               │ heap-buffer-overflow in extractImageSection()                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-3164                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-6228       │          │              │                         │               │ libtiff: heap-based buffer overflow in cpStripToTile() in    │
│                  │                     │          │              │                         │               │ tools/tiffcp.c                                               │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-6228                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libudev1         │ CVE-2013-4392       │          │              │ 252.17-1~deb12u1        │               │ TOCTOU race condition when updating file permissions and     │
│                  │                     │          │              │                         │               │ SELinux security contexts                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2013-4392                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31437      │          │              │                         │               │ An issue was discovered in systemd 253. An attacker can      │
│                  │                     │          │              │                         │               │ modify a...                                                  │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31437                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31438      │          │              │                         │               │ An issue was discovered in systemd 253. An attacker can      │
│                  │                     │          │              │                         │               │ truncate a...                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31438                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31439      │          │              │                         │               │ An issue was discovered in systemd 253. An attacker can      │
│                  │                     │          │              │                         │               │ modify the...                                                │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31439                   │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libuuid1         │ CVE-2022-0563       │          │              │ 2.38.1-5+b1             │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libxml2          │ CVE-2023-39615      │ MEDIUM   │              │ 2.9.14+dfsg-1.3~deb12u1 │               │ libxml2: crafted xml can cause global buffer overflow        │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-39615                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-45322      │          │              │                         │               │ libxml2: use-after-free in xmlUnlinkNode() in tree.c         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-45322                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ libxslt1.1       │ CVE-2015-9019       │ LOW      │              │ 1.1.35-1                │               │ libxslt: math.random() in xslt uses unseeded randomness      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2015-9019                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ login            │ CVE-2023-4641       │ MEDIUM   │              │ 1:4.13+dfsg1-1+b1       │               │ shadow-utils: possible password leak during passwd(1) change │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-4641                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2007-5686       │ LOW      │              │                         │               │ initscripts in rPath Linux 1 sets insecure permissions for   │
│                  │                     │          │              │                         │               │ the /var/lo ......                                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2007-5686                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-19882      │          │              │                         │               │ shadow-utils: local users can obtain root access because     │
│                  │                     │          │              │                         │               │ setuid programs are misconfigured...                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-19882                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-29383      │          │              │                         │               │ Improper input validation in shadow-utils package utility    │
│                  │                     │          │              │                         │               │ chfn                                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-29383                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ TEMP-0628843-DBAD28 │          │              │                         │               │ [more related to CVE-2005-4890]                              │
│                  │                     │          │              │                         │               │ https://security-tracker.debian.org/tracker/TEMP-0628843-DB- │
│                  │                     │          │              │                         │               │ AD28                                                         │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ mount            │ CVE-2022-0563       │          │              │ 2.38.1-5+b1             │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ nginx            │ CVE-2009-4487       │          │              │ 1.25.3-1~bookworm       │               │ nginx: Absent sanitation of escape sequences in web server   │
│                  │                     │          │              │                         │               │ log                                                          │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2009-4487                    │
│                  ├─────────────────────┤          ├──────────────┤                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2013-0337       │          │ will_not_fix │                         │               │ The default configuration of nginx, possibly 1.3.13 and      │
│                  │                     │          │              │                         │               │ earlier, uses ......                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2013-0337                    │
│                  ├─────────────────────┤          ├──────────────┤                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-44487      │          │ affected     │                         │               │ HTTP/2: Multiple HTTP/2 enabled web servers are vulnerable   │
│                  │                     │          │              │                         │               │ to a DDoS attack...                                          │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-44487                   │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ openssl          │ CVE-2023-5678       │ MEDIUM   │              │ 3.0.11-1~deb12u2        │               │ openssl: Generating excessively long X9.42 DH keys or        │
│                  │                     │          │              │                         │               │ checking excessively long X9.42...                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-5678                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2007-6755       │ LOW      │              │                         │               │ Dual_EC_DRBG: weak pseudo random number generator            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2007-6755                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2010-0928       │          │              │                         │               │ openssl: RSA authentication weakness                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2010-0928                    │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ passwd           │ CVE-2023-4641       │ MEDIUM   │              │ 1:4.13+dfsg1-1+b1       │               │ shadow-utils: possible password leak during passwd(1) change │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-4641                    │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2007-5686       │ LOW      │              │                         │               │ initscripts in rPath Linux 1 sets insecure permissions for   │
│                  │                     │          │              │                         │               │ the /var/lo ......                                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2007-5686                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2019-19882      │          │              │                         │               │ shadow-utils: local users can obtain root access because     │
│                  │                     │          │              │                         │               │ setuid programs are misconfigured...                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2019-19882                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-29383      │          │              │                         │               │ Improper input validation in shadow-utils package utility    │
│                  │                     │          │              │                         │               │ chfn                                                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-29383                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ TEMP-0628843-DBAD28 │          │              │                         │               │ [more related to CVE-2005-4890]                              │
│                  │                     │          │              │                         │               │ https://security-tracker.debian.org/tracker/TEMP-0628843-DB- │
│                  │                     │          │              │                         │               │ AD28                                                         │
├──────────────────┼─────────────────────┼──────────┤              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ perl-base        │ CVE-2023-31484      │ HIGH     │              │ 5.36.0-7                │               │ perl: CPAN.pm does not verify TLS certificates when          │
│                  │                     │          │              │                         │               │ downloading distributions over HTTPS...                      │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31484                   │
│                  ├─────────────────────┼──────────┤              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2011-4116       │ LOW      │              │                         │               │ perl: File::Temp insecure temporary file handling            │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2011-4116                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2023-31486      │          │              │                         │               │ http-tiny: insecure TLS cert default                         │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-31486                   │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ sysvinit-utils   │ TEMP-0517018-A83CE6 │          │              │ 3.06-4                  │               │ [sysvinit: no-root option in expert installer exposes        │
│                  │                     │          │              │                         │               │ locally exploitable security flaw]                           │
│                  │                     │          │              │                         │               │ https://security-tracker.debian.org/tracker/TEMP-0517018-A8- │
│                  │                     │          │              │                         │               │ 3CE6                                                         │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ tar              │ CVE-2005-2541       │          │              │ 1.34+dfsg-1.2           │               │ tar: does not properly warn the user when extracting setuid  │
│                  │                     │          │              │                         │               │ or setgid...                                                 │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2005-2541                    │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ CVE-2022-48303      │          │              │                         │               │ heap buffer overflow at from_header() in list.c via          │
│                  │                     │          │              │                         │               │ specially crafted checksum                                   │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-48303                   │
│                  ├─────────────────────┤          │              │                         ├───────────────┼──────────────────────────────────────────────────────────────┤
│                  │ TEMP-0290435-0B57B5 │          │              │                         │               │ [tar's rmt command may have undesired side effects]          │
│                  │                     │          │              │                         │               │ https://security-tracker.debian.org/tracker/TEMP-0290435-0B- │
│                  │                     │          │              │                         │               │ 57B5                                                         │
├──────────────────┼─────────────────────┤          │              ├─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ util-linux       │ CVE-2022-0563       │          │              │ 2.38.1-5+b1             │               │ util-linux: partial disclosure of arbitrary files in chfn    │
│                  │                     │          │              │                         │               │ and chsh when compiled...                                    │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├──────────────────┤                     │          │              │                         ├───────────────┤                                                              │
│ util-linux-extra │                     │          │              │                         │               │                                                              │
│                  │                     │          │              │                         │               │                                                              │
│                  │                     │          │              │                         │               │                                                              │
├──────────────────┼─────────────────────┼──────────┼──────────────┼─────────────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ zlib1g           │ CVE-2023-45853      │ CRITICAL │ will_not_fix │ 1:1.2.13.dfsg-1         │               │ zlib: integer overflow and resultant heap-based buffer       │
│                  │                     │          │              │                         │               │ overflow in zipOpenNewFileInZip4_6                           │
│                  │                     │          │              │                         │               │ https://avd.aquasec.com/nvd/cve-2023-45853                   │
└──────────────────┴─────────────────────┴──────────┴──────────────┴─────────────────────────┴───────────────┴──────────────────────────────────────────────────────────────┘
```
